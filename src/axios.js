// axios
import axios from 'axios'

const domain = "https://api.ecosyco.com"

export default axios.create({baseURL: domain, 
    params: {
     token: ''
    }
})

