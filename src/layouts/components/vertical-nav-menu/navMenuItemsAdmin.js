

export default [

  {
    header: "System",
    icon: "SystemIcon",
    i18n: "SystemEcosy",
    items: [
      {
        url: null,
        name: 'setting',
        icon: 'SettingsIcon',
        slug: 'setting',
        i18n: 'settingSystem',
        submenu: [
          {
            url: '/setting-crm',
            slug: 'setting-crm',
            name: 'setting-crm',
            i18n: 'dataSetting'
          },
        ]
      },
      {
        url: null,
        name: "store",
        icon: "ShoppingCartIcon",
        slug: "store",
        i18n: "store",
        submenu: [
          {
            url: '/cua-hang',
            slug: 'stores',
            name: 'stores',
            i18n: 'stores'
          },
        ]
      },
      {
        url: null,
        name: "syscustomers",
        icon: "UserIcon",
        slug: "syscustomers",
        i18n: "syscustomers",
        submenu: [
          {
            url: '/accounts',
            slug: 'accounts',
            name: 'accounts',
            i18n: 'accounts'
          },
          {
            url: '/khach-hang',
            slug: 'customers',
            name: 'datacustomers',
            i18n: 'datacustomers'
          },
        ]
      },
      {
        url: null,
        slug: 'services',
        name: 'services',
        i18n: 'services',
        icon: 'ServerIcon',
        submenu: [
          {
            url: '/dich-vu',
            slug: 'dataServices',
            name: 'dataServices',
            i18n: 'dataServices'
          },
        ]
      }
     
    ]
  },

]

