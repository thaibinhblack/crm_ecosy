

export default [

  {
    header: "store",
    icon: "CartIcon",
    i18n: "storeSystem",
    items: [
      {
        url: '/dashboard',
        name: "stdashboardore",
        icon: "ActivityIcon",
        slug: "dashboard",
        i18n: "dashboard",
      },
      {
        url: null,
        name: "store",
        icon: "ShoppingCartIcon",
        slug: "store",
        i18n: "store",
        submenu: [
          {
            url: '/cua-hang',
            slug: 'stores',
            name: 'stores',
            i18n: 'stores'
          },
          {
            url: '/cai-dat-cua-hang',
            slug: 'setting',
            name: 'setting',
            i18n: 'settingStore'
          },
          {
            url: '/nguoi-dung',
            slug: 'data-user-store',
            name: 'dataStoreUser',
            i18n: 'dataStoreUser'
          },
          {
            url: '/nguoi-dung/nhan-vien',
            slug: 'hrm-user-store',
            name: 'hrmUserStore',
            i18n: 'hrmUserStore'
          },
          {
            url: '/ip-cua-hang',
            slug: 'ip-store',
            name: 'settingIpStore',
            i18n: 'IpAddress'
          }
          // {
          //   url: '/nguoi-dung/them-moi',
          //   slug: 'add-user-store',
          //   name: 'addStoreUser',
          //   i18n: 'addStoreUser'
          // }
        ]
      },
      {
        url: null,
        slug: 'warehouse',
        name: 'warehouse',
        i18n: 'warehouse',
        icon: 'ArchiveIcon',
        submenu: [
          {
            url: '/san-pham',
            name: 'products',
            slug: 'products',
            i18n: 'DataProduct'
          },
          {
            url: '/san-pham/the-loai',
            name: 'productType',
            slug: 'productType',
            i18n: 'ProductType'
          }
        ]
      },
      {
        url: null,
        name: 'Hóa đơn',
        i18n: 'bill',
        icon: 'DollarSignIcon',
        submenu: [
          {
            url: '/hoa-don',
            slug: 'bills',
            name: 'Danh sách hóa đơn',
            i18n: 'bills'
          },
          {
            url: '/hoa-don/them-moi',
            slug: 'add-bill',
            name: 'Thêm mới hóa đơn',
            i18n: 'addBill'
          },
          {
            url: '/hoa-don/trang-thai',
            slug: 'status-bill',
            name: 'Trạng thái hóa đơn',
            i18n: 'statusBill'
          }
        ]
      },
      {
        url: null,
        name: 'syscustomers',
        i18n: 'syscustomers',
        icon: 'UserIcon',
        submenu: [
          {
            url: '/khach-hang',
            slug: 'customers',
            name: 'datacustomers',
            i18n: 'datacustomers'
          },
          {
            url: '/khach-hang/loai-khach-hang',
            slug: 'type-customer',
            name: 'typecustomer',
            i18n: 'typecustomer'
          },
          {
            url: '/dich-vu-khach-hang/cham-soc',
            slug: 'care-customer',
            name: 'carecustomer',
            i18n: 'carecustomer'
          },
          {
            url: '/cai-dat-thong-bao',
            slug: 'setting-notify',
            name: 'settingnotify',
            i18n: 'settingnotify'
          }
        ]
      },
      {
        url: null,
        name: 'sysmarketing',
        i18n: 'sysmarketing',
        icon: 'BookmarkIcon',
        submenu: [
          {
            url: '/quang-cao',
            slug: 'data-marketting',
            name: 'datamarketting',
            i18n: 'datamarketting'
          },
          {
            url: '/them-moi-quang-cao',
            slug: 'add-marketting',
            name: 'add-marketting',
            i18n: 'addmarketting'
          },
          {
            url: '/thong-bao',
            slug: 'data-notify',
            name: 'datanotify',
            i18n: 'datanotify'
          }
          // {
          //   url: '/markettings/contact',
          //   slug: 'ctonact',
          //   name: 'contact',
          //   i18n: 'contact'
          // }
        ],
      }
     
    ]
  },

]

