import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyBm2rMz9Y_CsdV3TLrGZaD_aZvWOm1B1Fw",
    authDomain: "ecosy-22213.firebaseapp.com",
    databaseURL: "https://ecosy-22213.firebaseio.com",
    projectId: "ecosy-22213",
    storageBucket: "ecosy-22213.appspot.com",
    messagingSenderId: "176570185287",
    appId: "1:176570185287:web:4600c476b6306601726362",
    measurementId: "G-E9K3HF7905"
  };

firebase.initializeApp(firebaseConfig)

export default firebase;