
import Vue from 'vue'
import Router from 'vue-router'
import axios from '@/axios.js'
import store from '@/store/store.js'
Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior () {
        return { x: 0, y: 0 }
    },
    routes: [

        {
    // =============================================================================
    // MAIN LAYOUT ROUTES
    // =============================================================================
            path: '',
            component: () => import('./layouts/main/Main.vue'),
            children: [
        // =============================================================================
        // Theme Routes
        // =============================================================================
                {
                    path: '/',
                    component: () => import('@/views/apps/dashboard/index.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Thống kê', url: '/dashboard' },
                            { title: 'Doanh thu ngày', active: true },
                        ],
                        pageTitle: 'Thống kê',
                        rule: 'editor'
                    }
                },
                {
                    path: '/dashboard',
                    component: () => import('@/views/apps/dashboard/index.vue'),
                    children:[
                       {
                            path: '',
                            name: 'THỐNG KÊ',
                            component: () => import('@/views/apps/dashboard/main.vue'),
                            meta: {
                                pageTitle: 'Thống kê',
                                rule: 'editor'
                            },
                        },
                        {
                            path: 'doanh-thu-ngay',
                            name: 'DOANH THU NGÀY',
                            component: () => import('@/views/apps/dashboard/dataDoanhThuNgay'),
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Thống kê', url: '/dashboard' },
                                    { title: 'Doanh thu ngày', active: true },
                                ],
                                pageTitle: 'Thống kê',
                                rule: 'editor'
                            },
                        }
                    ]
                },
                {
                    path: '/cua-hang',
                    name: 'DANH SÁCH CỬA HÀNG',
                    component: () => import('@/views/apps/store/DataStore.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Danh sách cửa hàng', active: true },
                        ],
                        pageTitle: 'Cửa hàng',
                        rule: 'editor'
                    },
                },
                {
                    path: '/cua-hang/:id',
                    name: 'CỬA HÀNG',
                    component: () => import('@/views/apps/store/DetailStore.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Danh sách cửa hàng', url: '/stores' },
                            { title: 'Cửa hàng', active: true },
                        ],
                        pageTitle: 'Cửa hàng',
                        rule: 'editor'
                    },
                },
                {
                    path: '/ip-cua-hang',
                    component: () => import('@/views/apps/store/dataIpAddress.vue'),
                    meta: {
                        pageTitle: 'Địa chỉ truy cập',
                        rule: 'editor'
                    },
                },
                {
                    path: '/hoa-don',
                    name: 'TRANG HÓA ĐƠN',
                    component: () => import('@/views/apps/bill/index.vue'),
                    children: [
                        {
                            path: '',
                            component: () => import('@/views/apps/bill/dataBill.vue'),
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách hóa đơn', active: true },
                                ],
                                pageTitle: 'Hóa đơn',
                                rule: 'editor'
                            },
                        },
                        {
                            path: 'them-moi',
                            component: () => import('@/views/apps/bill/addBill.vue'),
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách hóa đơn', url: '/apps/bills'},
                                    { title: 'Thêm mới hóa đơn', active: true },
                                ],
                                pageTitle: 'Hóa đơn',
                                rule: 'editor'
                            },
                        },
                        {
                            path: 'trang-thai',
                            component: () => import('@/views/apps/bill/dataStatus.vue'),
                            meta: {
                                rule: 'editor',
                                pageTitle: 'Danh sách trạng thái hóa đơn'
                            }
                        }

                    ]
                   
                },
                {
                    path: '/dich-vu-khach-hang',
                    component: () => import('@/views/apps/customer/index.vue'),
                    name: 'TRANG QUẢN LÝ KHÁCH HÀNG',
                    children: [
                        {
                            path: 'cham-soc',
                            component: () => import('@/views/apps/customer/careCustomer.vue'),
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách khách hàng', url: '/customers'},
                                    { title: 'Chăm sóc khách hàng', active: true}
                                ],
                                pageTitle: '',
                                rule: 'editor'
                            }
                        }
                    ]
                },
                {
                    path: '/khach-hang',
                    component: () => import('@/views/apps/customer/index.vue'),
                    name: 'TRANG QUẢN LÝ KHÁCH HÀNG',
                    children: [
                        {
                            path: '',
                            component: () => import('@/views/apps/customer/dataCustomer.vue'),
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách khách hàng', active: true},
                                   
                                ],
                                pageTitle: 'Danh sách khách hàng',
                                rule: 'editor'
                            }
                        },
                        {
                            path: 'loai-khach-hang',
                            component: () => import('@/views/apps/customer/typeCustomer.vue'),
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách khách hàng', url: '/customers'},
                                    { title: 'Phân loại khách hàng', active: true}
                                ],
                                pageTitle: 'Phân loại tập khách hàng',
                                rule: 'editor'
                            }
                        },
                        {
                            path: 'type/:id',
                            component: () => import('@/views/apps/customer/excelTypeCustomer.vue'),
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách khách hàng', url: '/customers'},
                                    { title: 'Phân loại khách hàng', url: '/customers/types'},
                                    { title: 'Danh sách phân loại khách hàng', active: true}
                                ],
                                pageTitle: 'Phân loại tập khách hàng',
                                rule: 'editor'
                            } 
                        },
                        {
                            path: ':UUID_KH',
                            component: () => import('@/views/apps/customer/detailCustomer.vue'),
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách khách hàng', url: '/customers'},
                                    { title: 'Chi tiết khách hàng', active: true}
                                ],
                                pageTitle: '',
                                rule: 'editor'
                            }
                        },
                    ]
                },
                {
                    path: '/cai-dat-thong-bao',
                    component: () => import('@/views/apps/setting/settingNotify.vue'),
                    meta: {
                        pageTitle: 'Cài đặt thông báo',
                        rule: 'editor'
                    }
                },
                {
                    path: '/accounts',
                    component: () => import('@/views/admin/account/index.vue'),
                    children: [
                        {
                            path: '',
                            component: () => import('@/views/admin/account/dataAccount.vue'),
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách người dùng', active: true},
                                ],
                                pageTitle: 'Người dùng',
                                rule: 'editor'
                            }
                        }
                    ]
                },
                {
                    path: '/account/:ID_USER',
                    component: () => import('@/views/admin/account/detailAccount.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Danh sách tài khoản', url: '/accounts'},
                            { title: 'Chi tiết tài khoản', active: true},
                        ],
                        pageTitle: 'Chi tiết tài khoản',
                        rule: 'editor'
                    },
                    children: [
                        {
                            path: '',
                            component:  () => import('@/views/admin/account/profile/infoProfile.vue'),
                            meta: {
                                rule: 'editor'
                            }
                        },
                        {
                            path: 'reset-password',
                            component:  () => import('@/views/admin/account/profile/resetPassword.vue'),
                            meta: {
                                rule: 'editor'
                            }
                        },
                        {
                            path: 'stores',
                            component:  () => import('@/views/admin/account/profile/myStore.vue'),
                            meta: {
                                rule: 'editor'
                            }
                        }
                    ]
                },
                {
                    path: '/marketting/:uuid',
                    component:() => import('@/views/apps/markettings/addMarketting.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Danh sách marketting', url: '/markettings'},
                            { title: 'Thông tin chi tiết', active: true}
                        ],
                        pageTitle: '',
                        rule: 'editor'
                    }
                
                },
                {
                    path: '/thong-bao',
                    component:() => import('@/views/apps/notify/index.vue'),
                    children: [
                        {
                            path: '',
                            component: () => import('@/views/apps/notify/dataNotify.vue'),
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách thông báo', active: true},
                                ],
                                pageTitle: 'Thông báo',
                                rule: 'editor'
                            }
                        }
                    ]
                },
                {
                    path: '/quang-cao',
                    component: () => import('@/views/apps/markettings/index.vue'),
                    children: [
                        {
                            path: '',
                            component: () => import('@/views/apps/markettings/dataMarketting.vue'),
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách marketting', active: true}
                                ],
                                pageTitle: '',
                                rule: 'editor'
                            }
                        },
                        {
                            path: 'contact',
                            component:() => import('@/views/apps/markettings/contactMarketting.vue'),
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách marketting', url: '/markettings'},
                                    { title: 'Thôn tin liên hệ', active: true}
                                ],
                                pageTitle: '',
                                rule: 'editor'
                            }
                        
                        }
                    ]
                },
                {
                    path: 'them-moi-quang-cao',
                    component:() => import('@/views/apps/markettings/addMarketting.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Danh sách marketting', url: '/markettings'},
                            { title: 'Thêm mới', active: true}
                        ],
                        pageTitle: '',
                        rule: 'editor'
                    }
                
                },
                {
                    path: '/profile',
                    component: () => import('@/views/pages/Profile.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Thông tin cá nhân', active: true}
                        ],
                        pageTitle: '',
                        rule: 'editor'
                    }
                },
                {
                    path: '/setting-crm',
                    component: () => import('@/views/admin/setting/index.vue'),
                    children: [
                       {
                           path: '/',
                           component: () => import('@/views/admin/setting/dataSetting/tableSetting.vue'),
                           meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách cài đặt hệ thống', active: true}
                                ],
                                pageTitle: '',
                                rule: 'editor'
                            }
                       }
                    ]
                },
                {
                    path: '/nguoi-dung',
                    component:() => import('@/views/apps/account/index.vue'),
                    children: [
                        {
                            path: '',
                            component: () => import('@/views/apps/account/dataAccountStore.vue'),
                            meta: {
                                pageTitle: 'Danh sách thành viên',
                                rule: 'editor'
                            }
                        },
                        {
                            path: 'nhan-vien',
                            component: () => import('@/views/apps/account/hrmAccountStore.vue'),
                            meta: {
                                pageTitle: 'Chấm công tháng',
                                rule: 'editor'
                            }
                        }
                    ]
                },
                {
                    path: '/dich-vu',
                    component: () => import('@/views/admin/services/index.vue'),
                    meta: {
                        pageTitle: 'Dịch vụ hệ thống',
                        rule: 'editor'
                    },
                    children: [
                        {
                            path: '',
                            component: () => import('@/views/admin/services/dataServices.vue'),
                            meta: {
                                pageTitle: 'Danh sách dịch vụ hệ thống',
                                rule: 'editor'
                            },
                        }
                    ]
                },
                {
                    path: '/san-pham',
                    component: () => import('@/views/apps/products/index.vue'),
                    children: [
                        {
                            path: '',
                            component: () => import('@/views/apps/products/data/DataProduct.vue'),
                            meta: {
                                pageTitle: 'Danh sách sản phẩm trong kho',
                                rule: 'editor'
                            }
                        },
                        {
                            path: 'the-loai',
                            component: () => import('@/views/apps/products/data/DataTypeProduct.vue'),
                            meta: {
                                pageTitle: 'Danh sách loại sản phẩm',
                                rule: 'editor'
                            }
                        },
                        {
                            path: '/cai-dat-cua-hang',
                            component: () => import('@/views/apps/store/pageSettingStore.vue'),
                            meta: {
                                pageTitle: 'Cài đặt cửa hàng',
                                rule: 'editor'
                            }
                        }
                    ]
                }
               

            ],
        },
    // =============================================================================
    // FULL PAGE LAYOUTS
    // =============================================================================
        {
            path: '',
            component: () => import('@/layouts/full-page/FullPage.vue'),
            children: [
        // =============================================================================
        // PAGES
        // =============================================================================
                {
                    path: '/callback',
                    name: 'auth-callback',
                    component: () => import('@/views/Callback.vue'),
                    meta: {
                        rule: 'editor'
                    }
                },
                {
                    path: '/login',
                    name: 'page-login',
                    component: () => import('@/views/pages/login/Login.vue'),
                    meta: {
                        rule: 'editor'
                    }
                },
                {
                    path:'/register',
                    component: () => import('@/views/pages/register/Register.vue'),
                    meta: {
                        rule: 'editor'
                    }
                   
                },
                {
                    path: '/access-email',
                    component: () => import('@/views/pages/register/AccessEmail.vue'),
                    meta: {
                        rule: 'editor'
                    }
                },
                {
                    path: '/reset-password',
                    component: () => import('@/views/pages/register/ResetPassword.vue'),
                    meta: {
                        rule: 'editor'
                    }
                },
                {
                    path: '/pages/error-404',
                    name: 'page-error-404',
                    component: () => import('@/views/pages/Error404.vue'),
                    meta: {
                        rule: 'editor'
                    }
                },
                {
                    path: '/pages/error-500',
                    name: 'page-error-500',
                    component: () => import('@/views/pages/Error500.vue'),
                    meta: {
                        rule: 'editor'
                    }
                },
            ]
        },
        //landing page
        {
            path: '',
            component: () => import('@/views/landingpage/index.vue'),
            children: [
                {
                    path: '/quang-cao/:uuid',
                    name: 'quang-cao',
                    component: () => import('@/views/landingpage/index.vue'),
                    meta: {
                        rule: 'editor'
                    }
                },
            ]
        },
        //offline
        {   
            path: '/offline',
            component: () => import('@/views/offline/index.vue'),
            meta: {
                rule: 'editor'
            }
        },
        {
            path: '/not-authorized',
            component: () => import('@/views/pages/NotAuthorized.vue'),
            meta: {
                rule: 'editor'
            }
        },
        {
            path: '/logout',
            component: () => import('@/views/pages/logout.vue'),
            meta: {
                rule: 'editor'
            }
        },
        // Redirect to 404 page, if no match found
        {
            path: '*',
            redirect: '/pages/error-404'
        }
    ],
})

router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById('loading-bg')
    if (appLoading) {
        appLoading.style.display = "none";
    }
})

router.beforeEach((to, from, next) => {
    const app = Vue;
    console.log(to,from)
    console.log(app.$cookies.isKey('token'))
    if(app.$cookies.isKey('token'))
    {
        axios.defaults.params.token = app.$cookies.get('token')
    }
    else
    {
        if(from.fullPath == '/access-email')
        {
            to.path === '/access-email'
        }
        else if(from.fullPath == '/register')
        {
            to.path === '/register'
        }
        else
        {
            to.path === '/login'
        }
    }
    return next();

});


export default router
