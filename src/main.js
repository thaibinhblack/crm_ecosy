
import Vue from 'vue'
import App from './App.vue'

// Vuesax Component Framework
import Vuesax from 'vuesax'
import 'material-icons/iconfont/material-icons.css' //Material Icons
import 'vuesax/dist/vuesax.css'; // Vuesax
Vue.use(Vuesax)
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';
import Vue2Editor from "vue2-editor";
import './assets/css/style.css'
import './assets/css/card.css'
import './assets/css/customer.css'
import { Datetime } from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css'
import '@/assets/css/tab.css'
import '@/assets/css/marketting.css';
import  { VueNestable, VueNestableHandle } from 'vue-nestable'
import VueSocketIO from 'vue-socket.io'
 
// Vue.use(new VueSocketIO({
//     debug: true,
//     connection: 'http://127.0.0.1:3000',
//     options: { path: "/" } //Optional options
// }))

Vue.component('VueNestable', VueNestable)
Vue.component('VueNestableHandle', VueNestableHandle)
Vue.component('datetime', Datetime);
import "@syncfusion/ej2-base/styles/material.css";
import "@syncfusion/ej2-vue-navigations/styles/material.css";
import "@syncfusion/ej2-inputs/styles/material.css";
import "@syncfusion/ej2-buttons/styles/material.css"
import { TreeViewComponent, TreeViewPlugin } from '@syncfusion/ej2-vue-navigations';

Vue.component(TreeViewPlugin.name, TreeViewComponent);
Vue.use(Vue2Editor);
Vue.component('v-select', vSelect)

import VueCookies from 'vue-cookies'

Vue.use(VueCookies)

// set default config
Vue.$cookies.config('1d')
// axios
import axios from "./axios.js"
Vue.prototype.$http = axios

// mock

// Theme Configurations
import '../themeConfig.js'


// ACL
import acl from './acl/acl'


// Globally Registered Components
import './globalComponents.js'


// Styles: SCSS
import './assets/scss/main.scss'


// Tailwind
import '@/assets/css/main.css'


// Vue Router
import router from './router'


// Vuex Store
import store from './store/store'


// i18n
import i18n from './i18n/i18n'


// Vuexy Admin Filters
import './filters/filters'


// Clipboard
import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard);


// Tour
import VueTour from 'vue-tour'
Vue.use(VueTour)
require('vue-tour/dist/vue-tour.css')


// VeeValidate
import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);


import AxiosPlugin from 'vue-axios-cors';
 
Vue.use(AxiosPlugin) 
// Vuejs - Vue wrapper for hammerjs
import { VueHammer } from 'vue2-hammer'
Vue.use(VueHammer)


// PrismJS
import 'prismjs'
import 'prismjs/themes/prism-tomorrow.css'


// Feather font icon
require('./assets/css/iconfont.css')


// Vue select css
// Note: In latest version you have to add it separately
// import 'vue-select/dist/vue-select.css';

//import firebase

import firebase from '@/services/firebase.js';
Vue.use(firebase)
//hot key
import VueHotkey from 'v-hotkey'
Vue.use(VueHotkey)

import VueMask from 'v-mask'
Vue.use(VueMask);

Vue.config.productionTip = false

new Vue({
    router,
    store,
    i18n,
    acl,
    render: h => h(App)
}).$mount('#app')
