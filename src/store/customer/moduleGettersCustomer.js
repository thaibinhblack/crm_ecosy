export default {
    LIST_CUSTOMER: state => {
        return state.customers
    },
    CUSTOMER_SEARCH: state => {
        return state.CUSTOMER_SEARCH
    },
    SETTING_PHAN_LOAI: state => {
        return state.SETTING_PHAN_LOAI
    },
    LIST_TYPE: state => {
        return state.LIST_TYPE
    },
    LIST_CUSTOMER_SYSTEM: state => {
        return state.LIST_CUSTOMER_SYSTEM
    },
    LIST_CUSTOMER_PUBLIC: state => {
        return state.LIST_CUSTOMER_PUBLIC
    },
    SELECT_CUSTOMER_STORE: state => {
        return state.SELECT_CUSTOMER_STORE
    },
    TYPE_CUSTOMER: state => {
        return state.TYPE_CUSTOMER
    },
    STATUS: state => {
        return state.STATUS
    },
    LIST_TYPE_CUSTOMER_STORE: state => {
        return state.LIST_TYPE_CUSTOMER_STORE
    }
}