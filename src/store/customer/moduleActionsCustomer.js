import axios from "@/axios"
import { resolve, reject } from "core-js/fn/promise";

export default {

    fetchCustomer({commit},filter)
    {
        var query_filter = ''
        if(filter != undefined)
        {
            query_filter = query_filter + `?filter=1&gender=${filter.GENDER}&money_min=${filter.SO_TIEN_MIN}&money_max=${filter.SO_TIEN_MAX}&sl_mua=${filter.SL_MUA}&age_min=${filter.ages[0]}&age_max=${filter.ages[1]}&ID_PROVINCE=${filter.ID_PROVINCE}&ID_DISTRICT=${filter.ID_DISTRICT}&ID_LOAI_CUA_HANG=${filter.TYPE_STORE}`;
        }
        return new Promise((resolve, reject) => {
            axios.get('/api/customers'+query_filter)
            .then((response) => {
                commit("SET_CUSTOMER",response.data.result)
                resolve(response)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchCustomerPublic({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/customers_public')
            .then((response) => {
               commit("SET_CUSTOMERS_PUBLIC",response.data.result)
               resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchCustomerStore({commit},filter)
    {
        var query_filter = ''
        if(filter != undefined)
        {
            query_filter = query_filter + `?filter=1&gender=${filter.GENDER}&money_min=${filter.SO_TIEN_MIN}&money_max=${filter.SO_TIEN_MAX}&sl_mua=${filter.SL_MUA}&age_min=${filter.ages[0]}&age_max=${filter.ages[1]}&ID_PROVINCE=${filter.ID_PROVINCE}&ID_DISTRICT=${filter.ID_DISTRICT}`;
        }
        return new Promise((resolve,reject) => {
            axios.get('/api/customer_ch'+query_filter)
            .then((response) => {
                commit("SET_CUSTOMER",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchCustomerByIDCH({commit},ID_CUA_HANG)
    {
        return new Promise((resolve, reject) => {
            axios.get('/api/customer_ch?ID_CUA_HANG='+ID_CUA_HANG)
            .then((response) => {
                commit("SET_CUSTOMER",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })

            axios.get('/api/type_customer')
            .then((response) => {
                if(response.data.success == true) commit("SET_LIST_TYPE_CUSTOMER",response.data.result);
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
            
        })
    },
    fetchCustomerBySDT({commit},SDT_KH)
    {
        return new Promise((resolve, reject) => {
            axios.get('/api/customers?SDT_KH='+SDT_KH)
            .then((response) => {
                commit("SET_CUSTOMER_SEARCH",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchCustomerByTypeStore({commit},TYPE_STORE)
    {
        return new Promise((resolve, reject) => {
            axios.get('/api/customers?TYPE_STORE='+TYPE_STORE)
            .then((response) => {
                commit("SET_CUSTOMER",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    createCustomer({commit},customer)
    {
        return new Promise((resolve, reject) => {
            if(customer.UUID_KH)
            {
                const form_customer = new FormData()
                form_customer.append("ID_CUA_HANG",customer.ID_CUA_HANG)
                form_customer.append("UUID_KH",customer.UUID_KH)
                axios.post('/api/customer_ch', form_customer)
                .then((response) => {
                    if(response.data.success == true)
                    {   
                        customer.CREATED_AT = response.data.result.CREATED_AT
                        commit("ADD_CUSTOMER",customer)
                    }
                    resolve(response.data)
                })
                .catch((err) => {
                    reject(err)
                })
            }
            else
            {
                const form_customer = new FormData()
                form_customer.append("TEN_KH",customer.TEN_KH)
                form_customer.append("NGAY_SINH_KH",customer.NGAY_SINH_KH)
                form_customer.append("SDT_KH",customer.SDT_KH)
                form_customer.append("DC_TP_KH",customer.DC_TP_KH)
                form_customer.append("DC_QH_KH",customer.DC_QH_KH)
                form_customer.append("DC_NHA_KH",customer.DC_NHA_KH)
                form_customer.append("ID_CUA_HANG",customer.ID_CUA_HANG)
                axios.post('/api/customer', form_customer)
                .then((response) => {
                    
                    customer.UUID_KH = response.data.result.UUID_KH
                    console.log('respone create',response.data.result, customer)
                    commit("ADD_CUSTOMER",customer)
                    resolve(response.data)
                    
                })
                .catch((err) => {
                    reject(err)
                })
            }
        })
    },
    deleteCustomerStore({commit},customer)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/customer_ch/'+customer.UUID_KH+'/destroy?ID_CUA_HANG='+customer.ID_CUA_HANG)
            .then((response) => {
                commit("DELETE_CUSTOMER",customer)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    resetCustomerStore({commit},customer)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/customer_ch/'+customer.UUID_KH+'/reset?ID_CUA_HANG='+customer.ID_CUA_HANG+'&type_reset='+customer.TYPE_RESET)
            .then((response) => {
                console.log('response',response.data.result)
                commit("ADD_CUSTOMER",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject({
                    success: false,
                    message: 'Lỗi server',
                    result: err,
                    status: 400
                })
            })
        })
    },
    //phân loại khách hàng
    fetchPhanLoaiKH({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/type_customer')
            .then((response) => {
               console.log('fetchPhanLoaiKH', response.data)
                commit("SET_TYPE_CUSTOMER",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    createPhanLoaiKH({commit},type_customer)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/type_customer',type_customer)
            .then((response) => {
                if(response.data.success == true)
                {
                    type_customer.ID_PHAN_LOAI = type_customer.ID_PHAN_LOAI;
                    commit("ADD_TYPE_CUSTOMER",type_customer);
                }
                resolve(response.data)
                
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    updatePhanLoaiKH({commit},type_customer)
    {
        return new Promise((resolve,reject) => {
            var form_type_customer = new FormData()
            form_type_customer.append("ID_CUA_HANG",type_customer.ID_CUA_HANG)
            form_type_customer.append("TEN_PHAN_LOAI",type_customer.TEN_PHAN_LOAI)
            form_type_customer.append("SO_TIEN_PHAN_LOAI_MIN",type_customer.SO_TIEN_PHAN_LOAI_MIN)
            form_type_customer.append("SO_TIEN_PHAN_LOAI_MAX",type_customer.SO_TIEN_PHAN_LOAI_MAX)
            form_type_customer.append("GHI_CHU",type_customer.GHI_CHU)
            axios.post('/api/type_customer/'+type_customer.ID_PHAN_LOAI+'/update',form_type_customer)
            .then((response) => {
                resolve(response.data)
                if(response.data.success == true)
                {
                    commit("UPDATE_TYPE_CUSTOMER",type_customer)
                }
               
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    //khách hàng hệ thống
    fetchCustomerSystem({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/customer_system')
            .then((response) => {
                resolve(response.data)
                
                if(response.data.success == true)
                {
                    console.log(response.data)
                    commit("SET_CUSTOMER_SYSTEM",response.data.result)
                }
                // console.log(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    SELECT_CUSTOMER_STORE({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/customer_ch/${data.customer.UUID_KH}?ID_CUA_HANG=${data.store.ID_CUA_HANG}`,{
                headers:{
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_SELECT_CUSTOMER",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    getTypeCustomer({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/type_customer/${data.ID_CUA_HANG}?UUID_KH=${data.UUID_KH}`).then((response) => {
                commit("SET_TYPE",response.data.result)
                resolve(response.data)
            }).catch((err) => {
                reject(err)
            })
        })
    },
    fetchTypeCustomers({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/type_customer')
            .then((response) => {
                if(response.data.success == true) commit("SET_LIST_TYPE_CUSTOMER",response.data.result);
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchTypeCustomerStore({commit}, filter)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/type_customer/${filter.ID_CUA_HANG}?ID_PHAN_LOAI=${filter.ID_PHAN_LOAI}`)
            .then((response) => {
                commit("SET_LIST_TYPE_CUSTOMER",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchBillCustomer({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/hoa-don/${data.UUID_KH}?ID_CUA_HANG=${data.ID_CUA_HANG}`)
            .then((response) => {
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    filterBillCustomer({commit},filter)
    {
        axios.post(`/api/hoa-don/${filter.UUID_KH}/filter`,filter,{
            headers: {
                Authorization: axios.defaults.params.token
            }
        })
        .then((response) => {
            commit("SET_BILL_CUSTOMER",response.data.result)
            resolve(response.data)
        })
        .catch((err) => {
            reject(err)
        })
    },  
    deleteTypeCustomer({commit},type)
    {
        return new Promise((resolve) => {
            axios.post(`/api/type_customer/${type.ID_PHAN_LOAI}/delete`)
            .then((response) => {
                commit("DELETE_TYPE_CUSTOMER",type)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchCustomerBirthdayInMonth({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/customer_ch?sort=birthday`)
            .then((response) => {
                commit("SET_CUSTOMER_BIRTHDAY",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    //cập nhật thông tin của khách hàng
    updateCustomerStore({commit},customer)
    {
        return new Promise((resolve,reject) => {
            axios.post(`/api/customer_ch/${customer.UUID_KH}/update`,customer)
            .then((response) => {
                commit("UPDATE_CUSTOMER_STORE",customer)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    }
}