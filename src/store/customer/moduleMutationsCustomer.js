export default {
    SET_CUSTOMER(state,customers)
    {
        state.customers = customers
    },
    ADD_CUSTOMER: (state,customer) => {
        state.customers.unshift(customer)
        state.LIST_CUSTOMER_PUBLIC.unshift(customer)
        // (state.LIST_CUSTUMER).push(customer)
    },
    SET_CUSTOMER_SEARCH(state,customer) {
        state.CUSTOMER_SEARCH = customer
    },
    DELETE_CUSTOMER(state,customer)
    {
        const IndexCustomer = state.LIST_CUSTOMER.findIndex((c) => c.UUID_KH == customer.UUID_KH && c.ID_CUA_HANG == customer.ID_CUA_HANG)
        state.LIST_CUSTOMER.splice(IndexCustomer,1)
    },
    SET_TYPE_CUSTOMER(state,LIST_TYPE)
    {
        console.log('mutation 2', LIST_TYPE)
        state.LIST_TYPE = LIST_TYPE
    },
    ADD_TYPE_CUSTOMER(state,type_customer)
    {
        state.LIST_TYPE.push(type_customer)
    },
    DELETE_TYPE_CUSTOMER(state,type_customer)
    {
        const IndexTypeCustomer = state.LIST_TYPE.findIndex((t) => t.ID_PHAN_LOAI == type_customer.ID_PHAN_LOAI)
        state.LIST_TYPE.splice(IndexTypeCustomer,1)
    },
    UPDATE_TYPE_CUSTOMER(state,type_customer)
    {
        const IndexType = state.LIST_TYPE.findIndex((type) => type.ID_PHAN_LOAI == type_customer.ID_PHAN_LOAI);
        Object.assign(state.LIST_TYPE[IndexType], type_customer)
    },
    SET_CUSTOMER_SYSTEM(state,customers)
    {
        console.log('customers',customers)
        state.LIST_CUSTOMER_SYSTEM = customers
    },
    SET_CUSTOMERS_PUBLIC(state,customers)
    {
        state.LIST_CUSTOMER_PUBLIC = customers
    },
    SET_SELECT_CUSTOMER(state,customer)
    {
        if(customer == null)
        {
            state.SELECT_CUSTOMER_STORE = {
                SO_TIEN_DA_CHI: 0,
                DIEM_TICH_LUY: 0
            }
        }
        else
        {
            state.SELECT_CUSTOMER_STORE = customer
        }
    },
    SET_TYPE(state,type)
    {
        if(type.TEN_PHAN_LOAI)
        {
            state.STATUS = type.TEN_PHAN_LOAI
        }
        else
        {
            state.STATUS = type
        }
    },
    SET_LIST_TYPE_CUSTOMER(state,customers)
    {
        state.LIST_TYPE_CUSTOMER_STORE = customers
    },
    SET_CUSTOMER_BIRTHDAY(state,customer_birthday)
    {
        state.LIST_CUSTOMER_BIRTHDAY = customer_birthday
    },
    UPDATE_CUSTOMER_STORE(state,customer)
    {
        console.log('LIST_CUSTOMER',state.customers)
        const IndexType = (state.customers).findIndex((customer_store) => customer_store.UUID_KH == customer.UUID_KH);
        Object.assign(state.customers[IndexType], customer)
    },
    //Danh sách hóa đơn của khách hàng
    SET_BILL_CUSTOMER(state,bills)
    {
        state.LIST_BILL_CUSTOMER = bills
    }

}