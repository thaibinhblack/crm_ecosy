import store from '@/store/store.js';
export default {
    LIST_BILL: state => {
        return state.bills
    },
    TOTAL_BILL: state => {
        var total = 0;
        state.bills.forEach((bill) => {
            total = total + (bill.GIA_SAN_PHAM * bill.SO_LUONG)
        })
        return total;
    },
    BILLS: state => {
        return state.LIST_BILL
    },
    LIST_LOG: state => {
        return state.LOGS
    },
    SETTING_BILL: state => {
        return state.SETTING_BILL
    },
    BILL_SEARCH: state => {
        return state.BILL_SEARCH
    },
    TOTAL_PRICE: state => {
        state.TOTAL_PRICE
    },
    BILL_TODAY: state => {
        return state.BILL_TODAY
    },
    //trạng thái hóa đơn
    LIST_STATUS_BILL: state => {
        return state.status_bill
    },
    TOTAL_BILL_NOT_PAY: state => {
        // if(state.LIST_BILL.length == 0)
        // {
        //     store.dispatch('fetchBill');
        // }
        // console.log('state ',state.LIST_BILL)
        const list_not_pay = state.LIST_BILL.filter((value,index,array) => {
            return array[index].STT_TRANG_THAI != -1 && array[index].STT_TRANG_THAI != 0
        });
        return list_not_pay.length;
    },
    LIST_BILL_NOT_PAY: state => {
        return state.LIST_BILL.filter((value,index,array) => {
            return array[index].STT_TRANG_THAI != -1 && array[index].STT_TRANG_THAI != 0
        })
    }
}