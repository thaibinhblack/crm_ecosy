// import store from '@/store/store'
export default {
    SET_BILL: (state,LIST_BILL) => {
        state.LIST_BILL = LIST_BILL
    },
    ADD_BILLS: (state,bill) =>
    {
        state.LIST_BILL.push(bill)
    },
    UPDATE_BILL_BY_ID(state,bill_update)
    {
        const IndexBillUpdate = state.LIST_BILL.findIndex((bill) => bill.ID_HOA_DON == bill_update.ID_HOA_DON);
        if(IndexBillUpdate != -1)
        {
            Object.assign(state.LIST_BILL[IndexBillUpdate],bill_update);
        }
        console.log('update bill',state.LIST_BILL)
    },
    ADD_BILL: (state,bill) => {
        var check_empty = 0;
        state.bills.forEach((bill_store) => {
            if(bill_store.ID_SAN_PHAM == bill.ID_SAN_PHAM)
            {
                check_empty = 1;
                bill_store.SO_LUONG = bill.SO_LUONG
            }
        })
        if(check_empty == 0 || state.bills.length == 0)
        {
            state.bills.push(bill)
        }
        
    },
    REMOVE_BILL: (state,ID_SAN_PHAM) => {
        
        const IndexBill = state.bills.findIndex((bill) => 
            bill.ID_SAN_PHAM == ID_SAN_PHAM
        )
    //     const ItemIndex = state.products.findIndex((p) => p.id == itemId)
    //   state.products.splice(ItemIndex, 1)
        // console.log('REMOVE',IndexBill,ID_SAN_PHAM)
        state.bills.splice(IndexBill,1)
        if(state.bills == null)
        {
            state.bills = []
        }
    },
    DELETE_BILL(state,ID_HOA_DON)
    {
        const IndexBill = state.LIST_BILL.findIndex((bill) => bill.ID_HOA_DON == ID_HOA_DON);
        state.LIST_BILL.splice(IndexBill,1)
    },
    //log
    SET_LOG(state,LOGS)
    {
        state.LOGS = LOGS
    },
    FilterBillByStore(state,store)
    {
        var result = []
        if(store)
        {
            result =  state.LIST_BILL.filter((value,index,array) => {
                return array[index].ID_CUA_HANG == store.ID_CUA_HANG
            })
        }
        else
        {
            result =  state.LIST_BILL
        }
        state.BILL_SEARCH = result
    },
    SET_BILL_TODAY(state,bills)
    {
        state.BILL_TODAY = bills
    },
    SET_STATUS_BILL(state,status_bill)
    {
        state.status_bill = status_bill;
    },
    ADD_STATUS_BILL(state,status_bill)
    {
        state.status_bill.push(status_bill);
    },
    UPDATE_STATUS_BILL(state,status_bill)
    {
        const IndexStatus = state.status_bill.findIndex((status) => status.ID_TRANG_THAI == status_bill.ID_TRANG_THAI);
        if(IndexStatus != -1)
        {
            Object.assign(state.status_bill[IndexStatus],status);
        }
    },
    UPDATE_DEFAULT_STATUS_BILL(state,id)
    {
        const IndexDefaultOld = state.status_bill.findIndex((status) => status.SELECTED == 1);
        state.status_bill[IndexDefaultOld].SELECTED = 0
        const IndexDefaultNew = state.status_bill.findIndex((status) => status.ID_TRANG_THAI == id);
        state.status_bill[IndexDefaultNew].SELECTED = 1;
    },
    UPDATE_STATUS_OF_BILL(state,data)
    {
        const IndexUpdate = state.LIST_BILL.findIndex((bill) => bill.ID_HOA_DON == data.ID_HOA_DON);
        state.LIST_BILL[IndexUpdate].ID_TRANG_THAI = data.STATUS;
        state.LIST_BILL[IndexUpdate].TEN_TRANG_THAI = data.TEN_TRANG_THAI;
    },
    UPDATE_TRANG_THAI(state,data)
    {
        console.log('trang thái',data);
        var list = data.LIST;
        var status_update = data.STATUS;
        const ItemTrangThai = state.status_bill.find((status) => status.ID_CUA_HANG == data.ID_CUA_HANG && status.STT == status_update);
        console.log('item ',ItemTrangThai, state.status_bill);
        if(ItemTrangThai != undefined) {
            list.forEach((id) => {
                const IndexBill = state.LIST_BILL.findIndex((bill) => bill.ID_HOA_DON == id);
                if(IndexBill != -1)
                {
                    const ItemBillLocal = {...state.LIST_BILL[IndexBill]};
                    ItemBillLocal.ID_TRANG_THAI = ItemTrangThai.ID_TRANG_THAI;
                    ItemBillLocal.STT_TRANG_THAI = data.STATUS;
                    console.log('updat trang trai',ItemBillLocal);
                    Object.assign(state.LIST_BILL[IndexBill],ItemBillLocal);
                }
            })
        }
    }
}