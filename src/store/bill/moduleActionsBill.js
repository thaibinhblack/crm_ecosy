import axios from '@/axios'
import { reject, resolve } from 'core-js/fn/promise';
import state from './moduleStateBill';
export default {
    addBill({commit},bill)
    {
        commit("ADD_BILL",bill)
    },
    removeBill({commit},ID_SAN_PHAM)
    {
        // console.log('ID_SP',ID_SAN_PHAM)
        commit("REMOVE_BILL", ID_SAN_PHAM)
    },
    fetchBill({commit},filter)
    {
        if(!filter)
        {
            
            return new Promise((resolve,reject) => {
                axios.get('/api/hoa-don',{
                    headers: {
                        Authorization: axios.defaults.params.token
                    }
                })
                .then((response) => {
                    commit("SET_BILL",response.data.result)
                    resolve(response.data)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        }
        else
        {
            return new Promise((resolve,reject) => {
                axios.get(`/api/hoa-don?ID_CUA_HANG=${filter.ID_CUA_HANG}&TIME_START=${filter.time_start}&TIME_END=${filter.time_end}`)
                .then((response) => {
                    commit("SET_BILL",response.data.result)
                    resolve(response.data)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        }
    },
    fetchBillToday({commit})
    {
        return new Promise((resolve,reject) => {
             axios.get('/api/hoa-don-ngay')
             .then((response) => {
                commit("SET_BILL_TODAY",response.data.result)
                resolve(response.data)
             })
             .catch((err) => {
                 reject(err)
             })
        })
    },
    createBill({commit},bill)
    {
        console.log('bill 123', bill)
        return new Promise((resolve, reject) => {
            bill.VALUE_HOA_DON = JSON.stringify(bill.VALUE_HOA_DON)
            axios.post('/api/hoa-don',bill)
            .then((response) => {
                if(response.data.success == true) 
                {
                    bill.ID_HOA_DON = response.data.result.ID_HOA_DON;
                    commit("ADD_BILLS",bill);
                    // state.LIST_BILL = [];
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    deleteBill({commit},ID_HOA_DON)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/hoa-don/'+ID_HOA_DON+'/delete')
            .then((response) => {
                commit("DELETE_BILL",ID_HOA_DON)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    updateStatusOfBill({commit},data)
    {
        return new Promise((resolve,reject) => {
            var status_selected = state.status_bill.find((status) => status.STT == data.STATUS);
            console.log('status ',status_selected)
            data.ID_TRANG_THAI = status_selected.ID_TRANG_THAI;
            data.TEN_TRANG_THAI = status_selected.TEN_TRANG_THAI;
            axios.post('/api/hoa-don-trang-thai/'+data.ID_HOA_DON,{
                STATUS: data.ID_TRANG_THAI
            })
            .then((response) => {
                if(response.data.success == true) commit('UPDATE_STATUS_OF_BILL',data);
                resolve(response.data)
            })
            .catch((err)=>{
                reject(err)
            })
        })
    },
    //log 

    fetchLogBill({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/log-hoa-don')
            .then((response) => {
                commit("SET_LOG",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    filterBillByStore({commit},sotre)
    {
        commit("FilterBillByStore",sotre);
    },
    //Trạng thái hóa đơn
    fetchStatusBill({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/trang-thai-hoa-don')
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("SET_STATUS_BILL", response.data.result);
                }
                resolve(response.data);
            })
            .catch((err) => {
                reject(err)
            });
        })
    },
    createStatusBill({commit},status)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/trang-thai-hoa-don',status)
            .then((response) => {
                if(response.data.success == true) 
                {
                    status.CREATED_AT = response.data.result.CREATED_AT;
                    commit("ADD_STATUS_BILL",status);
                }
                resolve(response.data);
            })
            .catch((err) => {
                reject(err);
            })
        })
    },
    updateStatusBill({commit},status)
    {
        return new Promise((resolve,reject) => {
            axios.post(`/api/trang-thai-hoa-don/${status.ID_TRANG_THAI}/update`)
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("UPDATE_STATUS_BILL",status);
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err);
            })
        })
    },
    defaultStatusBill({commit},id)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/trang-thai-hoa-don-default/'+id)
            .then((response) => {
                if(response.data.success == true) commit('UPDATE_DEFAULT_STATUS_BILL',id);
                resolve(response.data);
            })
            .catch((err) => {
                reject(err);
            })
        })
    },
    payAllBill({commit},data)
    {
        console.log('data ',data);
        return new Promise((resolve,reject) => {
            axios.post('/api/hoa-don-thanh-toan/'+data.ID_CUA_HANG,{
                LIST: JSON.stringify(data.LIST),
                STATUS: data.STATUS
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    commit('UPDATE_TRANG_THAI',data);
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    updateBill({commit},data)
    {
        return new Promise((resolve,reject) => {
            data.VALUE_HOA_DON = JSON.stringify(data.VALUE_HOA_DON);
            axios.post('/api/hoa-don/'+data.ID_HOA_DON+'/update_all',data)
            .then((response) => {
                if(response.data.success == true) commit('UPDATE_BILL_BY_ID',data);
                resolve(response.data)
            })
            .catch((err) => {
                reject(err);
            })

        })
    }
}