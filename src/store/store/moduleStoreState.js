export default {
    stores: [],
    managers: [],
    settings: {
        store: []
    },
    TYPE_STORE: [],
    SETTING_POINT: {
        SO_DIEM: 0,
        SO_TIEN: 0,
        DOI_TIEN: 0
    },
    STORE_DETAIL: {},
    list_ip: [],
    //setting notify
    list_notify: [],
    list_data_notify: [],
    setting_notify: {},
    list_setting_notify: []
}