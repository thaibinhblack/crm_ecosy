import axios from '@/axios'
import { reject, resolve } from 'core-js/fn/promise'
// import state from './moduleStoreState'
export default {

    fetchStore({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/stores').then((response) => {
                resolve(response.data)
                commit("SET_STORE",response.data.result)
            }).catch((error) => {
                reject(error)
            })
        })
    },
    fetchStoreByID({commit},ID_CUA_HANG)
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/store/'+ID_CUA_HANG, {
                headers: {
                    Authorization: axios.defaults.params.token
                }
            }).then((response) => {
                resolve(response.data)
                commit("SET_STORE_DETAIL",response.data.result)
            }).catch((error) => {
                reject(error)
            })
        })
    },
    createStore({commit}, store)
    {
        return new Promise((resolve,reject) => {
            const form_store = new FormData()
            form_store.append("ID_LOAI_CUA_HANG",store.ID_LOAI_CUA_HANG)
            form_store.append("TEN_CUA_HANG",store.TEN_CUA_HANG);
            form_store.append( "DIA_CHI_CUA_HANG",store.DIA_CHI_CUA_HANG);
            form_store.append("SDT_CUA_HANG",store.SDT_CUA_HANG);
            form_store.append("GHI_CHU",store.GHI_CHU);
            form_store.append("ID_PROVINCE",store.ID_PROVINCE)
            form_store.append("ID_DISTRICT",store.ID_DISTRICT)
            // form_store.append("OPTION_STORE",JSON.stringify(store.OPTION_STORE))
            axios.post('/api/store',form_store).then((response) => {
                if(response.data.success == true)
                {
                    store.ID_CUA_HANG = response.data.result.ID_CUA_HANG
                    commit("ADD_STORE",store)
                    
                }
                resolve(response.data)
            }).catch((error) => {
                reject(error)
            })
        })
    },
    updateStore({commit},store)
    {
        return new Promise((resolve,reject) => {
            const form_store = new FormData()
            form_store.append("TEN_CUA_HANG",store.TEN_CUA_HANG);
            form_store.append( "DIA_CHI_CUA_HANG",store.DIA_CHI_CUA_HANG);
            form_store.append("SDT_CUA_HANG",store.SDT_CUA_HANG);
            form_store.append("GHI_CHU",store.GHI_CHU);
            form_store.append("ID_PROVINCE",store.ID_PROVINCE)
            form_store.append("ID_DISTRICT",store.ID_DISTRICT)
            // form_store.append("OPTION_STORE",JSON.stringify(store.OPTION_STORE))
            axios.post('/api/store/'+store.ID_CUA_HANG+'/update',form_store).then((response) => {
                if(response.data.success == true)
                {
                    commit("UPDATE_STORE",store)
                    resolve(response.data)
                }
                else
                {
                    reject(response.data)
                }
            }).catch((error) => {
                reject({
                    success: false,
                    message: 'Lỗi server',
                    result: error,
                    status: 500
                })
            })
        })
    },
    deleteStore({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.delete(`/api/store/${data.ID_CUA_HANG}`)
            .then((response) => {
                if(response.success == true)
                {
                    commit("DELETE_STORE",data);
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            } )
        })
    },
    // manager
    fetchManagerStore({commit},ID_CUA_HANG)
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/manager_store?ID_CUA_HANG='+ID_CUA_HANG).then((response) => {
                if(response.data.success == true)
                {
                    commit("SET_MANAGER",response.data.result)
                    resolve(response.data.message)
                }
                else
                {
                    reject(response.data)
                }
            })
        })
    },
    createManagerStore({commit},data)
    {
        return new Promise((resolve,reject) => {
            console.log(data)
            data.array_user.forEach((user) => {
                
                const form_manager = new FormData();
                form_manager.append("ID_USER",user.ID_USER)
                form_manager.append("ID_CUA_HANG",data.id_cua_hang)
                axios.post('/api/manager_store',form_manager,{
                    headers: {
                        Authorization: axios.defaults.params.token
                    }
                }).then((response) => {
                    if(response.data.success == true)
                    {
                        commit("ADD_MANAGER",{
                            ID_CUA_HANG: data.id_cua_hang,
                            USER: user
                        })
                        resolve(response.data)
                    }
                    else
                    {
                        resolve(response.data)
                    }

                }).catch((error) => {
                    reject({
                        success: false,
                        message: 'Lỗi server',
                        result: error,
                        status: 500
                    })
                })
            })
        })
    },
    REMOVE_MANAGER({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/manager_store/'+data.ID_CUA_HANG+'/'+data.ID_USER+'/delete').then((response) => {
                if(response.data.success == true)
                {
                    commit("REMOVE_MANAGER",data)
                    resolve(response.data)
                }
                else
                {
                    reject(response.data)
                }
            }).catch((error) => {
                reject({
                    success: false,
                    message: 'Lỗi Server !',
                    result: error,
                    status: 500
                })
            })
        })
    },


    //type store

    fetchTypeStore({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/type-store')
            .then((response) => {
                commit("SET_TYPE_STORE",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },

    //seting point
    fetchPoint({commit},ID_CUA_HANG)
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/cai-dat-diem/'+ID_CUA_HANG, {
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                
               
                if(response.data.result != 'null')
                {
                    commit("SET_POINT",response.data.result)
                }
                else
                {
                    commit("SET_POINT",{
                        SO_TIEN: 0,
                        SO_DIEM: 0,
                        DOI_TIEN: 0
                    })
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    createPoint({commit},setting)
    {
        return new Promise((resolve,reject) => {
            const form_point = new FormData();
            form_point.append("ID_CUA_HANG",setting.ID_CUA_HANG)
            form_point.append("SO_DIEM",setting.SO_DIEM)
            form_point.append("SO_TIEN",setting.SO_TIEN)
            form_point.append("DOI_TIEN",setting.DOI_TIEN)
            axios.post('/api/cai-dat-diem',form_point)
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("SET_POINT",response.data.result)

                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },

    resignterStore({commit},data)
    {
        return new Promise((resolve,reject) => {
            const form_resign = new FormData();
            form_resign.append("USERNAME_USER",data.user.USERNAME_USER)
            form_resign.append("PASSWORD_USER",data.user.PASSWORD_USER)
            form_resign.append("HO_TEN_USER",data.user.HO_TEN_USER)
            form_resign.append("ID_LOAI_CUA_HANG",data.store.ID_LOAI_CUA_HANG)
            form_resign.append("TEN_CUA_HANG",data.store.TEN_CUA_HANG)
            form_resign.append("ID_PROVINCE",data.store.ID_PROVINCE)
            form_resign.append("ID_DISTRICT",data.store.ID_DISTRICT)
            form_resign.append("DIA_CHI_CUA_HANG",data.store.DIA_CHI_CUA_HANG)
            form_resign.append("SDT_CUA_HANG",data.store.SDT_CUA_HANG);
            form_resign.append("EMAIL_USER",data.user.EMAIL_USER);
            form_resign.append("ACCESS_PHONE",data.user.ACCESS_PHONE);
            axios.post('/api/register',form_resign).then((response) => {
                resolve(response.data)
                if(response.success == true)
                {
                    commit()
                }
                
            }).catch((err) => {
                reject(err)
            })
        })
    },
    ///Cài đặt ip truy cập\
    fetchIpStore({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/ip-store')
            .then((response) => {
                if(response.data.success == true) commit('SET_LIST_IP',response.data.result);
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    createIpStore({commit}, data)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/ip-store',data)
            .then((response) => {
                if(response.data.success == true)
                {
                    data.ID_IP = response.data.result.ID_IP;
                    commit('ADD_IP_STORE',data);
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    updateIpStore({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post(`/api/ip-store/${data.ID_IP}`,data)
            .then((response) => {
                if(response.data.success == true) commit('UPDATE_IP_STORE',data);
                resolve(response.data)
            })
            .catch(err => {
                reject(err);
            })
        })
    },
    deleteIpStore({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.delete(`/api/ip-store/${data.ID_IP}`)
            .then((response) => {
                if(response.data.success == true) commit('DELETE_IP_STORE',data);
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    //cài đặt thông báo
    fetchNotifyBirthday({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/setting-notify-birthday-data')
            .then((response) => {
                if(response.data.success == true) commit("SET_DATA_NOTIFY_BIRTHDAY",response.data.result.data);
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
            axios.get('/api/setting-notify-birthday')
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("SET_NOTIFY_BIRTHDAY",response.data.result)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err);
            })
        })
    },
    fetchSettingNotifyBirthday({commit},data)
    {
        return new Promise((resolve,reject) => {
            // console.log('data ',data)
            axios.get(`/api/setting-notify-birthday?ID_CUA_HANG=${data.ID_CUA_HANG}&ID_SETTING=${data.ID_SETTING}`)
            .then((response) => {
                if(response.data.success == true) 
                {
                    if(data.ID_SETTING == 0) commit("SET_SETTING_NOTIFY",response.data.result);
                    else commit("SET_LIST_SETTING_NOTIFY", response.data.result);
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err);
            })
        })
    },
    createNotify({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/setting-notify-birthday',data)
            .then((response) => {
                if(response.data.success == true){
                    data.ID_CUA_HANG = response.data.result.ID_CUA_HANG
                    if(data.TYPE_NOTIFY == 0) commit("ADD_SETTING_NOTIFY",data);
                    else commit("ADD_LIST_SETTING_NOTIFY",data);
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err);
            })
        })
    },
    updateNotifyBirthday({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post(`/api/setting-notify-birthday/${data.ID_SETTING_NOTIFY}/update`,data)
            .then((response) => {
                if(response.data.success == true)
                {
                    if(data.TYPE_NOTIFY == 0) commit("UPDATE_NOTIFY_BIRTHDAY",data);
                    else commit("UPDATE_LIST_NOTIFY",data);
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err);
            })
        })
    },
    deleteNotifyBirthday({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.delete(`/api/setting-notify-birthday/${data.ID_SETTING_NOTIFY}`)
            .then((response) => {
                if(response.data.success == true) commit("DELETE_NOTIFY_BIRTHDAY",data);
                resolve(response.data);
            })
            .catch((err) => {
                reject(err);
            })
        })
    }
}