export default {
    LIST_STORE: state => {
        return state.stores
    },
    LIST_MANAGER: state => {
        return state.managers
    },
    SETTINGS: state => {
        return state.settings
    },
    TYPE_STORE: state => {
        return state.TYPE_STORE
    },
    SETTING_POINT: state => {
        return state.SETTING_POINT
    },
    STORE_DETAIL: state => {
         return state.STORE_DETAIL
    },
    LIST_IP: state => {
        return state.list_ip;
    },
    LIST_NOTIFY: state => {
        return state.list_notify;
    },
    LIST_DATA_NOTIFY: state => {
        return state.list_data_notify;
    },
    SETTING_NOTIFY: state => {
        return state.setting_notify;
    },
    LIST_SETTING_NOTIFY: state => {
        return state.list_setting_notify;
    }
}