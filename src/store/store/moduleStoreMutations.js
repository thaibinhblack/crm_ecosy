export default {

    SET_STORE(state,stores)
    {
        state.stores = stores
    },

    ADD_STORE(state,store)
    {
        console.log('push',state.stores)
        state.stores.push(store)
    },

    UPDATE_STORE(state,store)
    {
        const storeIndex = state.stores.findIndex((s) => s.ID_CUA_HANG == store.ID_CUA_HANG)
        Object.assign(state.stores[storeIndex], store)
    },
    /// manager
    SET_MANAGER(state,managers)
    {
        state.managers = managers
    },
    ADD_MANAGER(state,manager)
    {

        state.managers.push(manager.USER)
        state.stores.filter((value,index,array) => {
            
            if(array[index].ID_CUA_HANG == manager.ID_CUA_HANG)
            {
                console.log( 'test',array[index])
                array[index].managers.push(manager.USER)
            }
        })
    },
    REMOVE_MANAGER(state,manager)
    {
        const store = state.stores.filter((value,index,array) => {
            return array[index].ID_CUA_HANG == manager.ID_CUA_HANG
        })[0]
        
        const ItemManagerIndex = store.managers.findIndex((m) => m.ID_USER == manager.ID_USER)
        store.managers.splice(ItemManagerIndex, 1)
        state.stores.filter((value,index,array) => {
            if(array[index].ID_CUA_HANG == store.ID_CUA_HANG)
            {
                array[index] = store
            }
        })
        const ItemManagerActionIndex = state.managers.findIndex((m) => m.ID_USER == manager.ID_USER)
        state.managers.splice(ItemManagerActionIndex,1)
    },


    //type store

    SET_TYPE_STORE(state,type_store)
    {
        state.TYPE_STORE = type_store
    },

    SET_POINT(state,SETTING_POINT)
    {
        state.SETTING_POINT = SETTING_POINT
    },
    SET_STORE_DETAIL(state,store)
    {
        state.STORE_DETAIL = store
    },
    DELETE_STORE(state,store)
    {
        const IndexStore = state.stores.findIndex((s) => s.ID_CUA_HANG == store.ID_CUA_HANG);
        if(IndexStore != -1)
        {
            state.stores.splice(IndexStore,1);
        }
    },
    //cài đặt địa chỉ truy cập
    SET_LIST_IP(state,list_ip)
    {
        state.list_ip = list_ip;
    },
    ADD_IP_STORE(state, ip)
    {
        state.list_ip.unshift(ip);
    },
    UPDATE_IP_STORE(state,ip)
    {
        const IndexIP = state.list_ip.findIndex((ip_store) => ip_store.ID_IP == ip.ID_IP);
        if(IndexIP != -1)
        {
            Object.assign(state.list_ip[IndexIP], ip);
        }
    },
    DELETE_IP_STORE(state,ip)
    {
        const IndexIP = state.list_ip.findIndex((ip_store) => ip_store.ID_IP == ip.ID_IP);
        if(IndexIP != -1)
        {
            state.list_ip.splice(IndexIP,1);
        }
    },
    //cài đặt thông báo
    SET_DATA_NOTIFY_BIRTHDAY(state,data_notify)
    {
        state.list_data_notify = data_notify
    },
    ADD_LIST_SETTING_NOTIFY(state,notify)
    {
        state.list_setting_notify.unshift(notify);
    },
    SET_LIST_SETTING_NOTIFY(state,list)
    {
        var list_set = list == null ? [] : list;
        state.list_setting_notify = list_set;
    },
    UPDATE_LIST_NOTIFY(state,notify)
    {
        let IndexNotify = state.list_setting_notify.findIndex((notify_update) => notify_update.ID_SETTING_NOTIFY == notify.ID_SETTING_NOTIFY);
        if(IndexNotify != -1)
        {
            Object.assign(state.list_setting_notify[IndexNotify],notify);
        }
    },
    SET_SETTING_NOTIFY(state,setting)
    {
        var data = setting == null  ? {} : setting
        state.setting_notify = data;
    },
    ADD_SETTING_NOTIFY(state,notify)
    {
        state.list_notify.unshift(notify);
    },
    SET_NOTIFY_BIRTHDAY(state,list_notify)
    {
        state.list_notify = list_notify;
    },
    UPDATE_NOTIFY_BIRTHDAY(state,notify)
    {
        const IndexNotify = state.list_notify.findIndex((notify_update) => notify_update.ID_SETTING_NOTIFY == notify.ID_SETTING_NOTIFY);
        if(IndexNotify != -1)
        {
            Object.assign(state.list_notify[IndexNotify],notify);
        }
    },
    DELETE_NOTIFY_BIRTHDAY(state,notify)
    {
        const IndexNotify = state.list_notify.findIndex((notify_delete) => notify_delete.ID_SETTING_NOTIFY == notify.ID_SETTING_NOTIFY)
        if(IndexNotify != -1)
        {
            state.list_notify.splice(IndexNotify,1);
        }
    }
}