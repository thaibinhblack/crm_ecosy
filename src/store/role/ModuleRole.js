import state from './ModuleRoleState'
import getters from './ModuleRoleGetters'
import mutations from './ModuleMutations'
import actions from './ModuleRoleActions'

export default {
    state,
    getters,
    mutations,
    actions
}