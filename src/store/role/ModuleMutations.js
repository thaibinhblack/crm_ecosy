export default {
    SET_GROUP_ROLE(state,group_roles)
    {
        state.group_roles = group_roles
    },
    ADD_GROUP_ROLE(state,group_role)
    {
        state.group_roles.push(group_role)
    },
    UPDATE_DETAIL_ROLE(state,detail_role)
    {
        const indexDetail = state.detail_roles.findIndex((dr) => dr.UUID_FUNCTION == detail_role.UUID_FUNCTION)
       
        if(indexDetail > -1)
        {
            state.detail_roles[indexDetail].roles = detail_role.roles
            // Object.assign(state.detail_roles[indexDetail].roles, detail_role.roles)
        }
        else
        {
            state.detail_roles.push(detail_role)
        }
        console.log(state.detail_roles)
       
    },
    SET_DETAIL_ROLES(state,detail_roles)
    {
        state.detail_roles = detail_roles
    }
   
}