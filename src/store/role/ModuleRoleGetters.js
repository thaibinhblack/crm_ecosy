import store from '@/store/store.js'
export default {
    group_roles: state => {
        var list_group_role = state.group_roles
        
        if(Object.entries(store.state.user.info).length > 0)
        {
            console.log('role', store.state.user.info, state.group_roles)
            if(store.state.user.info.UUID_GROUP_ROLE == store.state.UUID_GROUP_ROLE_ADMIN)
            {
                
                return list_group_role;
                
            }
            else
            {
    
                const IndexGroupRole = list_group_role.findIndex((role) => role.UUID_GROUP_ROLE == store.state.UUID_GROUP_ROLE_ADMIN)
                if(IndexGroupRole != -1)
                {
                    list_group_role.splice(IndexGroupRole,1)
                }
                
                return list_group_role;
            }
        }
        // return state.group_roles
    },
    detail_role(state, UUID_FUNCTION) {
        return state.detail_roles.filter((value,index,array) => {
            return array[index].UUID_FUNCTION == UUID_FUNCTION
        })[0]
    },
    detail_roles: state => {
        return state.detail_roles
    },
    
}