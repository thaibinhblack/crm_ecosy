import axios from '@/axios.js'
import state from './ModuleRoleState'
export default {
    fetchGroupRole({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/group-role',{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_GROUP_ROLE", response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    createGroupRole({commit},group_role)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/group-role',group_role,{
                headers: {
                    Authorization : axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("ADD_GROUP_ROLE",group_role)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    //DETAIL ROLE
    fetchDetailRoleByUUID({commit},UUID_GROUP_ROLE)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/detail-role/${UUID_GROUP_ROLE}`,{
                headers: {
                    Authorization : axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_DETAIL_ROLES",response.data.result)
                console.log(response.data)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchDetailRoleByIDUSER({commit},ID_USER)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/detail-role/${ID_USER}?type=ID_USER`,{
                headers: {
                    Authorization : axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_DETAIL_ROLES",response.data.result)
                console.log(response.data)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    }
    ,
    updateDetailRole({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post(`/api/detail-role/${data.ID}?type=${data.TYPE}`,state.detail_roles,{
                headers: {
                    Authorization : axios.defaults.params.token
                }
            })
            .then((response) => {
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },

}