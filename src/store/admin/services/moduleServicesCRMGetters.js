export default {
    group_services: state => {
        return state.group_services
    },
    services: state => {
        return state.services
    }
}