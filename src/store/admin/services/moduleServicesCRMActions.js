import axios from '@/axios.js';
import { reject, resolve } from 'core-js/fn/promise';
export default {
    fetchGroupServices({commit},filter)
    {
        var filter_api = filter == undefined ? '' : filter;
        return new Promise((resolve,reject) => {
            axios.get(`/api/group-services${filter_api}`)
            .then((response) => {
                if(response.data.success == true)
                {
                    commit('SET_GROUP_SERVICES',response.data.result);
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    createGroupService({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/group-services',data)
            .then((response) => {
                if(response.data.success == true)
                {
                    data.ID_GROUP_SERVICES = response.data.result.id;
                    data.CREATED_AT = response.data.result.created_at;
                    commit("ADD_GROUP_SERVICES",data);
                }
                resolve(response.data);
            })
            .catch((err) => {
                console.log(err)
                reject(err);
            })
        });
    },
    updateGroupService({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post(`/api/group-service/${data.ID_GROUP_SERVICES}/update`,data)
            .then((response) => {
                if(response.data.success == true)
                {
                    commit('UPDATE_GROUP_SERVICE',data)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    //serivces
    fetchServices({commit},filter)
    {
        var filter_api = filter == undefined ? '' : filter;
        return new Promise((resolve,reject) => {
            axios.get(`/api/services-ecosy${filter_api}`)
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("SET_SERVICES",response.data.result)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    createServices({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/services-ecosy',data)
            .then((response) => {
                if(response.data.success == true)
                {
                    data.UUID_SERVICES = response.data.result.UUID_SERVICES;
                    data.CREATED_AT = response.data.result.created_at;
                    commit("ADD_SERVICES",data);
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    updateService({commit},service)
    {
        return new Promise((resolve,reject) => {
            axios.post(`/api/services-ecosy/${service.UUID_SERVICES}/update`,service)
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("UPDATE_SERVICE",service)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    deleteService({commit},UUID_SERVICES)
    {
        return new Promise((resolve,reject) => {
            axios.delete(`/api/services-ecosy/${UUID_SERVICES}`)
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("DELETE_SERVICES",UUID_SERVICES);
                }
                resolve(response.data);
            })
            .catch((err) => {
                reject(err);
            })
        })
    }
}