import state from './moduleServicesCRMState.js'
import getters from './moduleServicesCRMGetters.js'
import mutations from './moduleServicesCRMMutations.js'
import actions from './moduleServicesCRMActions.js'

export default {
    state,
    getters,
    mutations,
    actions,
}