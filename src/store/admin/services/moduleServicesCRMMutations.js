export default {
    SET_GROUP_SERVICES(state,group_services)
    {
        state.group_services = group_services
    },
    ADD_GROUP_SERVICES(state,group_service)
    {
        state.group_services.unshift(group_service)
    },
    UPDATE_GROUP_SERVICE(state,group_service)
    {
        const IndexGroupService = state.group_services.findIndex((gs) => gs.ID_GROUP_SERVICES == group_service.ID_GROUP_SERVICES);
        Object.assign(state.group_services[IndexGroupService],group_service);
    },
    //services
    SET_SERVICES(state,services)
    {
        state.services = services
    },
    ADD_SERVICES(state,service)
    {
        state.services.unshift(service)
    },
    UPDATE_SERVICE(state,service)
    {
        const IndexService = state.services.findIndex((s) => s.UUID_SERVICES = service.UUID_SERVICES);
        Object.assign(state.services[IndexService],service)
    },
    DELETE_SERVICES(state,UUID_SERVICES)
    {
        const IndexService = state.services.findIndex((s) => s.UUID_SERVICES = UUID_SERVICES);
        if(IndexService != -1)
        {
            state.services.splice(IndexService,1);
        }
    }
}