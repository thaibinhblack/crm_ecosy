import axios from '@/axios.js'
import state from './moduleAdminState'
export default {
    //account
    FetchAccount({commit},pagination)
    {
        return new Promise((resolve,reject) => {
            var url = '/api/account-auth';
            if(pagination)
            {
                if(pagination.current_page)
                {
                    url = `/api/account-auth?page=${pagination.current_page}`;
                    if(pagination.show)
                    {
                        url = `/api/account-auth?page=${pagination.current_page}&show=${pagination.show}`;
                    }
                }
            }
            axios.get(url,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_LIST_ACCOUNT", response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchAccountByID({commit},ID_USER)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/account-auth/${ID_USER}`,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit('SET_DETAIL_ACCOUNT',response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    //NHÓM CHỨC NĂNG
    fetchGroupFunction({commit})
    {
        return new Promise((resolve,reject) => {
           if(state.functions.length == 0 )
           {
                axios.get('/api/group-auth',{
                    headers: {
                        Authorization: axios.defaults.params.token
                    }
                })
                .then((response) => {
                    commit("SET_GROUP_FUNCTION",response.data.result)
                    resolve(response.data)
                })
                .catch((err) => {
                    reject(err)
                })
           }
           else
           {
               resolve({
                   mesage: 'DANH SÁCH CHỨC NĂNG',
                   success: true,
                   result: state.functions
               })
           }
        })
    },
    createGroupFunction({commit},group)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/group-auth',group,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("ADD_GROUP_FUNCTION",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    //CHỨC NĂNG
    fetchFunction({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/function-auth',{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_FUNCTION",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    createFunction({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/function-auth',data,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                data.UUID_FUNCTION = response.data.result
                commit('ADD_FUNCTION',data)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    createAccount({commit},user)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/account-auth',user,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    user.ID_USER = response.data.result
                    commit("ADD_ACCOUNT",user)
                }
                resolve(response.data)
            })
            .catch((er) => {
                reject(err)
            })
        })
    },
    updateAccount({commit},user)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/account-auth/'+user.ID_USER,user,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("UPDATE_ACCOUNT",user)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    lockAccount({commit},user)
    {
        return new Promise((resolve,reject) => {
            var SATUS_UPDATE = user.STATUS == 1 ? 0 : 1;
            axios.post('/api/account-auth-lock/'+user.ID_USER,{
                STATUS: SATUS_UPDATE
            },{
                headers: {
                    Authorization : axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    user.STATUS = SATUS_UPDATE;
                    commit("UPDATE_ACCOUNT",user)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    accessEmail({commit},user)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/account-auth/'+user.ID_USER+'?access_email=1',user,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    user.ACCESS_EMAIL = 1;
                    commit("UPDATE_ACCOUNT",user)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    //fetch store
    fetchStoreByID_USER({commit},ID_USER)
    {
        return new Promise((resolve, reject) => {
            axios.get('/api/store-auth/'+ID_USER,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_MY_STORE",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    //store

    createStoreAuth({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/store-auth/'+data.ID_USER+'/create',data.store,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    data.store.ID_CUA_HANG = response.data.result
                    data.store.customers = []
                    commit('ADD_MY_STORE',data.store);
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    //cài đặt
    fetchSettingCRM({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/setting-crm',{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_SETTING_CRM",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    createSettingCRM({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/setting-crm',data,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                data.ID_ECOSY_CRM = response.data.result
                commit('ADD_SETTING',data)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchUserByRole({commit},role)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/account-auth?UUID_ROLE=${role}`,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                
                // console.log('Danh sách người dùng ', response.data.success)
                if(response.data.success == true)
                {
                    // console.log('Danh sách người dùng ', response.data)
                    commit("SET_ACCOUNT_ROLE",response.data.result)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    }
}