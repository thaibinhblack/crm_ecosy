import state from './moduleAdminState'
import getters from './ModuleAdminGetters'
import mutations from './ModuleAdminMutations'
import actions from './ModuleAdminActions'
import ModuleServicesCRM from './services/moduleServicesCRM'
export default {
    state,
    getters,
    mutations,
    actions,
}