export default {
    SET_LIST_ACCOUNT(state,accounts) {
        state.accounts = accounts
    },
    //chức năng
    SET_GROUP_FUNCTION(state,groups)
    {
        state.groups = groups
    },
    ADD_GROUP_FUNCTION(state,group)
    {
        state.groups.push(group)
    },
    SET_FUNCTION(state,functions)
    {
        state.functions = functions
    },
    ADD_FUNCTION(state,data)
    {
        state.functions.push(data)
    },
    ADD_ACCOUNT(state,account)
    {
        state.accounts.unshift(account)
    },
    UPDATE_ACCOUNT(state,account)
    {
        if(state.accounts.length > 0)
        {
            const IndexAccount = state.accounts.findIndex((account) => account.ID_USER == account.ID_USER)
            if(IndexAccount != -1)
            {
                Object.assign(state.accounts[IndexAccount],account)
            }
        }
    },
    SET_DETAIL_ACCOUNT(state,account)
    {
        state.account_detail = account
    },
    SET_MY_STORE(state,my_store)
    {
        state.my_stores = my_store
    },
    ADD_MY_STORE(state,store)
    {
        state.my_stores.unshift(store)
    },
    SET_SETTING_CRM(state,settings)
    {
        state.settingAuth = settings
    },
    ADD_SETTING(state,setting)
    {
        state.settingAuth.unshift(setting)
    },
    SET_ACCOUNT_ROLE(state,accounts)
    {
        state.accounts_role = accounts
    }
}