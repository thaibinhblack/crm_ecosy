export default {
    SET_PRODUCT(state,products) {
        state.LIST_PRODUCT = products
    },
    ADD_PRODUCT(state,product){
        state.LIST_PRODUCT.push(product)
    },
    DELETE_PRODUCT(state,ID_SAN_PHAM)
    {
        const IndexProduct = state.LIST_PRODUCT.findIndex((product) => product.ID_SAN_PHAM == ID_SAN_PHAM)
        console.log(ID_SAN_PHAM,IndexProduct)
        state.LIST_PRODUCT.splice(IndexProduct,1)
    },
    UPDATE_SO_LUONG(state,LIST_PRODUCT)
    {
        LIST_PRODUCT.forEach(product => {
            const IndexProduct = state.LIST_PRODUCT.findIndex((p) => p.ID_SAN_PHAM == product.ID_SAN_PHAM);
            if(IndexProduct != -1)
            {
                var productUpdate = {...state.LIST_PRODUCT[IndexProduct]}
                productUpdate.SO_LUONG = productUpdate.SO_LUONG - product.SO_LUONG
                Object.assign(state.LIST_PRODUCT[IndexProduct],productUpdate);
            }
        });
    }
}