import state from './moduleWarehouseState'
import getters from './moduleWarehouseGetters'
import actions from './moduleWarehouseActions'
import mutations from './moduleWarehouseMutations'

export default {
    state,
    getters,
    mutations,
    actions
}