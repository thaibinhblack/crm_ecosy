export default {
    UPDATE_SIDEBAR_MEDIA(state,boolean)
    {
        state.sidebarMedia = boolean
    },
    UPDATE_SINGLE_MEDIA(state,boolean)
    {
        state.single = boolean
    },
    CHECK_MEDIA(state,ID_MEDIA)
    {
        if(state.single == true)
        {
           state.media_single = ID_MEDIA
        }
        else
        {
            state.media.push(ID_MEDIA)
        }
       
    },
    REMOVE_MEDIA(state,media)
    {
        const IndexMedia = state.media.findIndex(ID_MEDIA => ID_MEDIA == media );
        if(IndexMedia != -1) state.media.splice(IndexMedia,1)
    },
    EMPTY_MEDIA(state,array_media)
    {
        state.media = array_media;
    },
    EMPTY_MEDIA_SINGLE(state,media_single)
    {
        state.media_single = media_single
        // console.log('media ',state.media)
    },
    //type product
    SET_TYPE_PRODUCT(state,type_products)
    {
        state.type_products = type_products;
    },
    ADD_TYPE_PRODUCT(state,type_product)
    {
        if(type_product.PARENT_PRODUCT_TYPE == 0) {
            type_product.child = [];
            state.type_products.push(type_product);
        }
        else{
            const IndexTypeProduct = state.type_products.findIndex((type) => type.ID_PRODUCT_TYPE == type_product.PARENT_PRODUCT_TYPE);
            if(IndexTypeProduct != -1) state.type_products[IndexTypeProduct].child.push(type_product);
            console.log('result ',state.type_products,IndexTypeProduct , type_product)
        }
        
    },
    UPDATE_TYPE_PRODUCT(state,type_product)
    {
        var IndexTypeProduct = state.type_products.findIndex((type) => type.ID_TYPE_PRODUCT == type_product.ID_TYPE_PRODUCT);
        if(type_product.PARENT_PRODUCT_TYPE == 0)
        {
            if(IndexTypeProduct != -1) Object.assign(state.type_products[IndexTypeProduct],type_product);       
        }
        else
        {
            //trường hợp là gốc
            if(IndexTypeProduct == -1) 
            {
                state.type_products.concat(type_product.child);
                state.type_products.splice(IndexTypeProduct,1);
                type_product.child = null
                const IndexTypeProductParent = state.type_products.findIndex(type => type.ID_PRODUCT_TYPE == type_product.PARENT_PRODUCT_TYPE);
                state.type_products[IndexTypeProductParent].child.push(type_product);
            }
            //trường hợp là con
            else
            {
                const IndexTypeProductParent = state.type_products.findIndex(type => type.ID_PRODUCT_TYPE == type_product.PARENT_PRODUCT_TYPE);
                const IndexTypeProductParentOf = state.type_products.findIndex((type) => type.ID_PRODUCT_TYPE == type_product.PARENT_PRODUCT_TYPE_OLD);
                if(IndexTypeProductParentOf != -1) {
                    console.log('product type' ,state.type_products[IndexTypeProductParentOf]);
                   if(state.type_products[IndexTypeProductParentOf].child.length > 0)
                   {
                    const IndexTypeProductChild = state.type_products[IndexTypeProductParentOf].child.findIndex(type => type.ID_PRODUCT_TYPE == type_product.ID_PRODUCT_TYPE);
                    if(IndexTypeProductChild != -1) 
                        state.type_products[IndexTypeProductParentOf].child.splice(IndexTypeProductChild,1);
                        Object.assign(state.type_products[IndexTypeProductParentOf],state.type_products[IndexTypeProductParentOf])
                    }
                }
                state.type_products[IndexTypeProductParent].child.push(type_product);
            }


        }
    },
    DELETE_TYPE_PRODUCT(state,ID_TYPE_PRODUCT)
    {
        const IndexTypeProduct = state.type_products.findIndex((type) => type.ID_TYPE_PRODUCT == ID_TYPE_PRODUCT);
        if(IndexTypeProduct != -1) state.type_products.splice(IndexTypeProduct,1);
    },
    CHECK_TYPE_PRODUCT(state,data)
    {
        if(data.CHECK) {
            const IndexTypeProduct = state.CHECK_TYPE_PRODUCT.findIndex((ID_PRODUCT_TYPE) => ID_PRODUCT_TYPE == data.ID_PRODUCT_TYPE);
            if(IndexTypeProduct == -1) state.CHECK_TYPE_PRODUCT.push(data.ID_PRODUCT_TYPE);
        }
        else {
            var IndexTypeProduct = state.CHECK_TYPE_PRODUCT.findIndex((ID_PRODUCT_TYPE) => ID_PRODUCT_TYPE == data.ID_PRODUCT_TYPE);
            if(IndexTypeProduct != -1) state.CHECK_TYPE_PRODUCT.splice(IndexTypeProduct,1);
            console.log('remove type product', state.CHECK_TYPE_PRODUCT, IndexTypeProduct, data)
        }
    },
    RESET_CHECK_TYPE_PRODUCT(state,boolean)
    {
        console.log('reset ',state.reset_check_type_product, boolean)
        state.reset_check_type_product = boolean
    },
    //product
    SET_PRODUCT(state,products)
    {
        console.log('set ',products)
        state.products = products.data
    },
    ADD_PRODUCT(state,product)
    {
        state.products.unshift(product);
    },
    UPDATE_PRODUCT(state,product)
    {
        console.log('update ',product);
        const IndexProduct = state.products.findIndex((p) => p.ID_SAN_PHAM == product.ID_SAN_PHAM);
        if(IndexProduct != -1) Object.assign(state.products[IndexProduct],product);
    }
}