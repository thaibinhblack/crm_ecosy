export default {
    media_store: state => {
        return state.media
    },
    single: state => {
        return state.single
    },
    media_single: state => {
        return state.media_single
    },
    LIST_TYPE_PRODUCT: state => {
        return state.type_products;
    },
    CHECK_TYPE_PRODUCT: state => {
        return state.CHECK_TYPE_PRODUCT
    },
    reset_check_type_product: state => {
        return state.reset_check_type_product
    },
    LIST_PRODUCT: state => {
        return state.products;
    }
}