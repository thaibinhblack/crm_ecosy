export default {
    sidebarMedia: false,
    single: false,
    media: [],
    media_single: null,
    type_products: [],
    CHECK_TYPE_PRODUCT: [],
    products: [],
    reset_check_type_product: false,
}