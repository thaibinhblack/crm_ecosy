import axios from '@/axios.js'
import state from './moduleWarehouseState'
export default {
    //type product
    fetchTypeProduct({commit},filter)
    {   
        return new Promise((resolve,reject) => {
            
            if(state.type_products.length == 0)
            {
                axios.get(`/api/type-product`,filter)
                .then((response) => {
                    if(response.data.success == true)
                    {
                        commit("SET_TYPE_PRODUCT",response.data.result);
                    }
                    resolve(response.data)
                })
                .catch((err) => {
                    reject(err)
                })
            }
            else
            {
                resolve({
                    success: true,
                    message: 'Danh sách loại sản phẩm của cửa hàng',
                    result: state.type_products,
                    status: 200
                });
            }
        })
    },
    createTypeProduct({commit},type_product)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/type-product',type_product)
            .then((response) => {
                if(response.data.success == true)
                {
                    type_product.ID_PRODUCT_TYPE = response.data.result.ID_PRODUCT_TYPE
                    commit("ADD_TYPE_PRODUCT",type_product)
                }
                resolve(response.data)
            })
            .catch((err) => {
                console.log('err ',err)
                reject(err)
            });
        })
    },
    updateTypeProduct({commit},type_product)
    {
        return new Promise((resolve,reject) => {
            axios.post(`/api/type-product/${type_product.ID_PRODUCT_TYPE}/update`,type_product)
            .then((response) => {
                // if(response.data.success == true)
                // {
                //     commit("UPDATE_TYPE_PRODUCT",type_product)
                // }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            });
        })
    },
    deleteTypeProduct({commit},ID_TYPE_PRODUCT)
    {
        return new Promise((resolve,reject) => {
            axios.delete(`/api/type-product/${ID_TYPE_PRODUCT}`)
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("DELETE_TYPE_PRODUCT",ID_TYPE_PRODUCT)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            });
        })
    },
    //product
    fetchProduct({commit},filter)
    {
        console.log('filter ',filter)
        return new Promise((resolve,reject) => {
            axios.get(`/api/products`,filter)
            .then((response) => {
                if(response.data.success == true)
                {
                    // commit('SET_PAGE_PRODUCT',{
                        
                    // })
                    commit('SET_PRODUCT',response.data.result);
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err);
            })
        })
    },
    createProduct({commit},data)
    {
        return new Promise((resolve,reject) => {
            data.LIST_TYPE_PRODUCT = state.CHECK_TYPE_PRODUCT
            axios.post('/api/product',data)
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("ADD_PRODUCT",response.data.result)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    updateProduct({commit},data)
    {
        return new Promise((resolve,reject) => {
            data.LIST_TYPE_PRODUCT = state.CHECK_TYPE_PRODUCT
            axios.post(`/api/product/${data.ID_SAN_PHAM}/update`,data)
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("UPDATE_PRODUCT",response.data.result)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    checkCodeProduct({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/check-code-product/${data.MA_SAN_PHAM}?ID_CUA_HANG=${data.ID_CUA_HANG}`)
            .then((response) => {
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    }
}