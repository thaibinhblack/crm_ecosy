export default {
    SET_MENU_FILTER(state,menus)
    {
        state.menu_filter = menus
    },
    SET_MENU(state,menus)
    {
        state.menus = menus
    },
    ADD_MENU(state,menu)
    {
        if(menu.MENU_PARENT == null)
        {
            state.menus.push(menu)
        }   
        else
        {
            const indexMenu = state.menus.findIndex((m) => m.UUID_MENU == menu.MENU_PARENT)
            state.menus[indexMenu].children.push(menu)
        }
        console.log('menu ', state.menus)
    },
    UPDATE_MENU(state,menu)
    {
        console.log('menu ',menu)
        if(menu.MENU_PARENT != null )
        {
           const indexMenuParent = state.menus.findIndex((m) => m.UUID_MENU == menu.MENU_PARENT)
           const indexMenuChildren = state.menus.children.findIndex((children) => children.UUID_MENU == menu.UUID_MENU)
           state.menus[indexMenuParent].children[indexMenuChildren] = menu
        }
        else
        {
            const indexMenu = state.menus.findIndex((m) => m.UUID_MENU == menu.UUID_MENU)
            state.menus[indexMenu].NAME_MENU = menu.NAME_MENU
            state.menus[indexMenu].DESC_MENU = menu.DESC_MENU
            state.menus[indexMenu].STT_MENU = menu.STT_MENU
        }
        console.log(' state menu ',state.menus)
       
    },
    EDIT_MENU(state,menu)
    {
        state.menu_edit = menu
    },
    DELETE_MENU(state,menu)
    {
        if(menu.UUID_MENU != null)
        {
            const IndexParent = state.menus.findIndex((m) => m.UUID_MENU == menu.MENU_PARENT)
            const IndexChildren = state.menus[indexMenuParent].children.findIndex((c) => c.UUID_MENU == menu.UUID_MENU)
            state.menus[IndexParent].children.splice(IndexChildren,1)
        }
        else
        {
            const IndexMenu = state.menus.findIndex((m) => m.UUID_MENU == menu.UUID_MENU)
            state.menus.splice(IndexMenu,1)
        }
    },
    ADD_MENU_DETAIL(state,UUID_MENU)
    {
        if(state.check_create_menu_user == true)
        {
            state.create_menu_user.push(UUID_MENU)
        }
        else
        {
            state.list_menu_detail.push(UUID_MENU)
        }
    },
    REMOVE_MENU_DETAIL(state,UUID_MENU)
    {
        
        if(state.check_create_menu_user == true)
        {
            const IndexDetail = state.list_menu_detail.findIndex((UUID) => UUID == UUID_MENU)
            state.list_menu_detail.splice(IndexDetail,1) 
        }
        else
        {   
            const IndexDetail = state.list_menu_detail.findIndex((UUID) => UUID == UUID_MENU)
            state.list_menu_detail.splice(IndexDetail,1) 
        }
        
    },
    SET_MENU_USER(state,list_menus)
    {
        if(state.menu_user.length > 0 )
        {
            state.menu_user.forEach((menu) => {
                const IndexMenuUser = state.list_menu_detail.findIndex((menu_detail) => menu_detail == menu)
                state.list_menu_detail.splice(IndexMenuUser,1)
            })
            state.menu_user = []
        }
        var list_menu_user = [];
        list_menus.forEach((menu) => {
            list_menu_user.push(menu.UUID_MENU)
        });
        state.menu_user = list_menu_user
        var list_menu_detail_tmp = []
        state.list_menu_detail.forEach((menu) => {
            const IndexMenuTmp = list_menu_user.findIndex((UUID_MENU) => UUID_MENU == menu)
            if(IndexMenuTmp != -1)
            {
                list_menu_user.splice(IndexMenuTmp,1)
            }
        })
        list_menu_user.forEach((UUID_MENU) => {
            state.list_menu_by_role.push(UUID_MENU)
        })
        
        console.log('menu tmp',list_menu_user, state.list_menu_by_role)
        // (state.list_menu_detail).concat(list_menu_user)
    },
    UPDATE_CHECK_MENU_USER(state,boolean)
    {
        state.check_create_menu_user = boolean
    },
    SET_LIST_MENU(state,list_menu)
    {
        const array_menu_by_role = []
        list_menu.forEach(menu => {
            array_menu_by_role.push(menu.UUID_MENU)
        });
        state.list_menu_by_role = array_menu_by_role
    },
    UPDATE_MENU_USER(state)
    {
        state.menu_user.forEach((menu) => {
            const IndexMenuRole = state.list_menu_by_role.findIndex((menu_role) => menu == menu_role)
            state.list_menu_by_role.splice(IndexMenuRole,1)
        })
        state.menu_user = []
    }
}