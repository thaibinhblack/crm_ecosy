import state from './ModuleMenuState'
import getters from './ModuleMenuGetters'
import mutations from './ModuleMenuMutations'
import actions from './ModuleMenuActions'

export default {
    state,
    getters,
    mutations,
    actions
}