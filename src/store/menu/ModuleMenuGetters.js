export default {
    menu_parent: state => {
        var menus =  state.menus
        return menus.filter((value,index,array) => {
            return array[index].MENU_PARENT == null
        })
    },
    menus: state => {
        return state.menus
    },
    menu_edit: state => {
        return state.menu_edit
    },
    list_menu_detail: state => {
        return state.list_menu_detail
    },
    list_menu_by_role: state => {
        return state.list_menu_by_role
    }
}