import axios from '@/axios.js'
import state from './ModuleMenuState'
export default {
    fetchMenu({commit},filter = null)
    {
        if(filter != undefined && filter != null)
        {
            return new Promise((resolve,reject) => {
                axios.get(`/api/menu?filter=${filter.type}`,{
                    headers: {
                        Authorization: axios.defaults.params.token
                    }
                })
                .then((response) => {
                    commit("SET_MENU_FILTER", response.data.result)
                    resolve(response.data)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        }
        else
        {
            return new Promise((resolve,reject) => {
                axios.get(`/api/menu`,{
                    headers: {
                        Authorization: axios.defaults.params.token
                    }
                })
                .then((response) => {
                    commit("SET_MENU", response.data.result)
                    resolve(response.data)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        }
    },
    createMenu({commit},menu)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/menu',menu,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    menu.UUID_MENU = response.data.result
                    menu.children = []
                    commit("ADD_MENU",menu)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    updateMenu({commit},menu)
    {
        return new Promise((resolve,reject) => {
            axios.post(`/api/menu/${menu.UUID_MENU}`,menu,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    console.log('before update', menu)
                    commit("UPDATE_MENU",menu)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    deleteMenu({commit},menu)
    {
        return new Promise((resolve,reject) => {
            axios.post(`/api/menu/${menu.UUID_MENU}/destroy`,menu,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.success == true)
                {
                    commit("DELETE_MENU",menu)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    //detail
    createDetailMenu({commit},UUID_GROUP_ROLE)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/detail-menu',{
                UUID_GROUP_ROLE : UUID_GROUP_ROLE,
                LIST_UUID_MENU : state.list_menu_detail
            },
            {
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                resolve(response.data)
            })
            .catch((err) => {
                console.log('api err',err)
                reject(err)
            })
        })
    },
    createMenuUser({commit},ID_USER)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/detail-menu-user/'+ID_USER,
            {
                UUID_GROUP_ROLE : null,
                LIST_UUID_MENU : state.create_menu_user
            },
            {
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                // commit("SET_MENU_CREATE")
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchMenuUser({commit},ID_USER)
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/detail-menu-user/'+ID_USER,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_MENU_USER",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchMenuByRole({commit},UUID_GROUP_ROLE)
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/group-role/'+UUID_GROUP_ROLE+'/menu',{
                headers: {
                    Authorization : axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_LIST_MENU",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    }
}