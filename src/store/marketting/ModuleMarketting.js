import state from './ModuleSatetMarketting'
import getters from './ModuleGettersMarketting'
import mutations from './ModuleMutationsMarketting'
import actions from './ModuleActionsMarketting'

export default {
    state,
    getters,
    mutations,
    actions
}