import axios from '@/axios'
import { reject } from 'core-js/fn/promise'
export default {
    fetchMarketting({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/marketting`,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_MARKETTINGS",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchMarkettingByStore({commit},ID_CUA_HANG)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/marketting/${ID_CUA_HANG}`,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_MARKETTINGS",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    submitMarketting({commit},data)
    {
        return new Promise((resolve,reject) => {
            const form_marketting = new FormData();
            form_marketting.append("ID_CUA_HANG",data.marketting.ID_CUA_HANG)
            form_marketting.append("BANNER_MARKETTING",data.marketting.BANNER_MARKETTING)
            form_marketting.append("NAME_MARKETTING",data.marketting.NAME_MARKETTING)
            form_marketting.append("CONTENT_MARKETTING",data.marketting.CONTENT_MARKETTING)
            form_marketting.append("TIME_START_MARKETTING",`${new Date(data.marketting.TIME_START_MARKETTING).getFullYear()}-${(new Date(data.marketting.TIME_START_MARKETTING).getMonth() + 1)}-${new Date(data.marketting.TIME_START_MARKETTING).getDate()} ${new Date(data.marketting.TIME_START_MARKETTING).getHours()}:${new Date(data.marketting.TIME_START_MARKETTING).getMinutes()}:00`)
            form_marketting.append("TIME_END_MARKETTING",`${new Date(data.marketting.TIME_END_MARKETTING).getFullYear()}-${(new Date(data.marketting.TIME_END_MARKETTING).getMonth() + 1)}-${new Date(data.marketting.TIME_END_MARKETTING).getDate()} ${new Date(data.marketting.TIME_END_MARKETTING).getHours()}:${new Date(data.marketting.TIME_END_MARKETTING).getMinutes()}:00`)
            axios.post('/api/marketting',form_marketting)
            .then((response) => {
                console.log('response ',response)
                // var form_marketting_product = new FormData();
                // form_marketting_product.append("UUID_MARKETTING",response.data.result)
                // form_marketting_product.append("ID_CUA_HANG",data.marketting.ID_CUA_HANG)
                // form_marketting_product.append("TEN_SAN_PHAM",data.product.TEN_SAN_PHAM)
                // form_marketting_product.append("GIA_SAN_PHAM",data.product.GIA_SAN_PHAM)
                // form_marketting_product.append("SO_LUONG_BAN",data.product.SO_LUONG_BAN)
                // form_marketting_product.append("GIAM_GIA",data.product.GIAM_GIA)
                // form_marketting_product.append("NOI_DUNG_SAN_PHAM",data.product.NOI_DUNG_SAN_PHAM)
                // form_marketting_product.append("HINH_ANH_DAI_DIEN",data.product.HINH_ANH_DAI_DIEN)
                // axios.post('/api/marketting-product',form_marketting_product,
                // {
                //     headers: {
                //         Authorization: axios.defaults.params.token
                //     }
                // })
                // .then((response) => {
                //     console.log(response.data)
                // })
                resolve(response.data)
            })
            .catch((err) => {
                console.log(err);
                reject(err)
            })
        })
    },
    updateContactMarketting({commit},contact)
    {
        return new Promise((resolve,reject) => {
            var form_contact = new FormData()
            form_contact.append("ID_CUA_HANG",contact.ID_CUA_HANG)
            form_contact.append("ADDRESS_CONTACT",contact.ADDRESS_CONTACT)
            form_contact.append("PHONE_CONTACT",contact.PHONE_CONTACT)
            form_contact.append("EMAIL_CONTACT",contact.EMAIL_CONTACT)
            form_contact.append("WEBSITE_CONTACT",contact.WEBSITE_CONTACT)
            form_contact.append("MAP_CONTACT",contact.MAP_CONTACT)
            axios.post('/api/contact-marketting',form_contact,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_CONTACT_STORE",contact)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchContactMarkettingByID({commit},ID_CUA_HANG)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/contact-marketting/${ID_CUA_HANG}`)
            .then((response) => {
                commit("SET_CONTACT_STORE",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    }
}