import firebase from '@/firebase.js'

export default {
    sentOTP({commit},data)
    {
        return new Promise((resolve,reject) => {
            console.log(data)
            if((data.SDT_USER).length > 10 ){
                alert('Invalid Phone Number Format !');
              }else{
                //
                let countryCode = '+84' //vn  
                let phoneNumber = countryCode + data.SDT_USER
                //
                let appVerifier = data.appVerifier
                firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
                  .then(function (confirmationResult) {
                    window.confirmationResult = confirmationResult;
                    resolve(response)
                  }).catch(function (error) {
                    reject(error)
                });
              }
        })
    },
    initReCaptcha({commit}){
        return new Promise((resolve,reject) => {
            setTimeout(()=>{
                window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
                'size': 'invisible',
                'callback': function(response) {
                  
                },
                'expired-callback': function() {
                    // Response expired. Ask user to solve reCAPTCHA again.
                    // ...
                }
                });
                //
                resolve(window.recaptchaVerifier)
            },1000)
        })
    }
}