import state from './moduleServicesState'
import getters from './moduleServicesGetters'
import mutations from './moduleServicesMutations'
import actions from './moduleServicesActions'

export default {
    state,
    getters,
    mutations,
    actions
}