import axios from '@/axios.js'
import { reject, resolve } from 'core-js/fn/promise'
export default {
    fetchHRM({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/user-store-hrm',{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("SET_LIST_HRM",response.data.result)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchDetailHRM({commit},filter)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/user-store-hrm/${filter.ID_USER}?NGAY_CHAM_CONG_HRM=${filter.NGAY_CHAM_CONG_HRM}`,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("SET_DETAIL_HRM",response.data.result)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    createUpdateHRM({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/user-store-hrm',data, {
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("ADD_HRM",data)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchUserHRM({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/user-store',{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("SET_LIST_USER_HRM",response.data.result)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    createUserHRM({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post('/api/user-store',data,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("ADD_USER_HRM",data)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    updateUserHRM({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post(`/api/user-store/${data.ID_USER_HRM}/update`,data,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("UPDATE_USER_HRM",data)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    deleteUserHRM({commit},ID_USER_HRM)
    {
        return new Promise((resolve,reject) => {
            axios.delete(`/api/user-store/${ID_USER_HRM}`,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                if(response.data.success == true)
                {
                    commit("DELETE_USER_HRM_STORE",ID_USER_HRM)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    //role
    fetchRoleDefault({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/role-default')
            .then((response) => {
                if(response.data.success == true) commit('SET_ROLE_DEFAULT',response.data.result);
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    //Tài khoản nhân viên
    createAccountUser({commit},data)
    {
        return new Promise((resolve,reject) => {
            axios.post(`/api/user-account-hrm/${data.ID_USER_HRM}`,data)
            .then((response) => {
               
                if(response.data.success == true) 
                {
                    data.ID_USER = response.data.result.ID_USER;
                    commit('ADD_ACCOUNT_USER_HRM',data);
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err);
            })
        })
    }
}