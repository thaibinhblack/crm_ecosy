export default {
    ADD_HRM(state,hrm)
    {
        const IndexHRM = state.list_hrm.findIndex((hrm_data) => hrm_data.ID_USER == hrm.ID_USER && hrm_data.NGAY_CHAM_CONG_HRM == hrm.NGAY_CHAM_CONG_HRM);
        if(IndexHRM != -1 )
        {
            Object.assign(state.list_hrm[IndexHRM],hrm);
        }
        else
        {
            state.list_hrm.push(hrm)
        }
    },
    SET_LIST_HRM(state,hrms)
    {
        state.list_hrm = hrms
    },
    SET_DETAIL_HRM(state,hrm_detail)
    {
        state.hrm_detail = hrm_detail
    },
    SET_LIST_USER_HRM(state,users)
    {
        state.list_user_hrm = users
    },
    ADD_USER_HRM(state,user)
    {
        state.list_user_hrm.push(user)
    },
    UPDATE_USER_HRM(state,user)
    {
        const IndexHRM = state.list_user_hrm.findIndex((hrm) => hrm.ID_USER_HRM == user.ID_USER_HRM);
        if(IndexHRM !=-1)
        {
            Object.assign(state.list_user_hrm[IndexHRM],user)
        }
    },
    DELETE_USER_HRM_STORE(state,ID_USER_HRM)
    {
        const IndexHRM = state.list_user_hrm.findIndex((hrm) => hrm.ID_USER_HRM == ID_USER_HRM);
        if(IndexHRM != -1)
        {
            state.list_user_hrm.splice(IndexHRM,1)
        }
    },
    EMPTY_DETAIL_HRM(state,detail)
    {
        state.hrm_detail = detail
    },
    SET_ROLE_DEFAULT(state,list_role)
    {
        state.list_role_default = list_role;
    },
    ADD_ACCOUNT_USER_HRM(state,user_hrm)
    {
        const IndexHRM = state.list_user_hrm.findIndex((hrm) => hrm.ID_USER_HRM == user_hrm.ID_USER_HRM);
        if(IndexHRM != -1)
        {
            Object.assign(state.list_user_hrm[IndexHRM],user_hrm);
        }
    }
}