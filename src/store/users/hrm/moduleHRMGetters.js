export default {
    hrm_detail: state => {
        if(state.hrm_detail != null)
        {
            return state.hrm_detail;
        }
        else
        {
            return {};
        }
    },
    LIST_ROLE_DEFAULT: state => {
        return state.list_role_default;
    }
}