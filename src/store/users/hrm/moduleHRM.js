import state from './moduleHRMState'
import getters from './moduleHRMGetters'
import mutations from './moduleHRMMutations'
import actions from './moduleHRMActions'

export default {
    state,
    getters,
    mutations,
    actions
}