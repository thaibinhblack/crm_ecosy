export default {
    SET_USERS(state,users)
    {   
        state.users = users
    },
    ADD_TOKEN(state,token)
    {
        state.token = token
    },
    SET_AUTH(state,auth)
    {
        state.auth = auth
    },
    UPDATE_USER(state,user)
    {
        const userIndex = state.users.findIndex((u) => u.ID_USER == user.ID_USER)
        Object.assign(state.users[userIndex], user)
    },
    SET_LIST_USER_BY_ROLE(state,list_user_role)
    {
        console.log('list user role ',list_user_role)
        state.list_user_role = list_user_role
    },
    SET_MENU_USER(state,menus)
    {
        state.menu_user = menu
    },
    SET_ACOUNT_STORE(state,accounts)
    {
        console.log('danh sách user',accounts)
        state.accounts_store = accounts
    },
    SET_ROLE_DEFAULT(state,roles)
    {
        console.log('roles', roles)
        state.role_default = roles
    },
    ADD_ACCOUNT_STORE(state,account)
    {
        state.list_account_store.push(account)
    },
    SET_INFO(state,info)
    {
        state.info = info
    },
    SET_ADMIN(state,boolean)
    {
        state.admin = boolean
    }
}