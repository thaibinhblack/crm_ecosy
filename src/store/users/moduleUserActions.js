import axios from '@/axios.js'
export default {
    fetchUsers({commit}){
        return new Promise((resolve, reject) => {
            axios.get("/api/users",{
              headers: {
                Authorization: axios.defaults.params.token
             }
            })
              .then((response) => {
                commit('SET_USERS', response.data.result)
                resolve(response.data)
              })
              .catch((error) => { reject(error) })
          })
    },
    fetchUserByGroupRole({commit}, UUID_GROUP_ROLE)
    {
      return new Promise((resolve,reject) => {
        axios.get('/api/group-role/'+UUID_GROUP_ROLE+'/users',{
          headers: {
            Authorization: axios.defaults.params.token
          }
        })
        .then((response) => {
          commit('SET_LIST_USER_BY_ROLE',response.data.result)
          resolve(response.data)
        })
        .catch((err) => {
          reject((err))
        })
      })
    },
    
    loginSystem({commit},user)
    {
      // console.log('login',user)
      return new Promise((resolve,reject) => {
        const form_login = new FormData();
        form_login.append("USERNAME_USER",user.USERNAME_USER)
        form_login.append("PASSWORD_USER",user.PASSWORD_USER)
        axios.post('/api/login-crm',form_login).then((response) => {
          
          if(response.data.success == true)
          {
            //set header token
            axios.defaults.headers.common['Authorization'] = response.data.result
            // this.$store.state.user.info = response.data.info
            localStorage.setItem('token', response.data.result)
            localStorage.setItem('admin', response.data.admin)
            commit("SET_INFO",response.data.info)
            commit("SET_ADMIN",response.data.admin)
            commit("ADD_TOKEN",response.data.result)
          }
          resolve(response.data)
         
        }).catch((err) => {
          localStorage.removeItem('token')
          reject(err)
        })
      })
    },
    checkUser({commit},token)
    {
      return new Promise((resolve,reject) => {
        console.log('token',token)
        axios.get('/api/info').then((response) => {
          // axios.defaults.params.token = token
          commit("SET_AUTH",response.data.result)
          resolve(response.data)
        }).catch(() => {
          axios.defaults.params.token = null
          commit("SET_AUTH",null)
          reject({
            success: false,
            message: 'User này không tồn tại'
          })
        })
      })
    },
    updateInfo({commit},user)
    { 
      return new Promise((resolve,reject) => {
        axios.post('/api/info/update',user)
        .then((response) => {
          if(response.data.success == true) commit("SET_INFO",response.data.result);
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        })
      })
    },
    submitUser({commit},user)
    {
      return new Promise((resolve,reject) => {
          var form_user = new FormData();
          
          if(user.ID_USER)
          {
            form_user.append("GT_USER",user.GT_USER)
            form_user.append("HO_TEN_USER",user.HO_TEN_USER)
            form_user.append("AVATAR",user.AVATAR)
            form_user.append("BIRTH_DAY",user.BIRTH_DAY)
            form_user.append("SDT_USER",user.SDT_USER)
            form_user.append("DC_USER",user.DC_USER)

            axios.post('/api/user/'+user.ID_USER,form_user,{
              headers: {
                Authorization: token
              }
            })
            .then((response) => {
              resolve(response.data)
              commit("UPDATE_USER",user)
            })
            .catch((err) => {
              reject(err)
            })
          }
          else
          {
            form_user.append("USERNAME_USER",user.USERNAME_USER)
            form_user.append("PASSWORD_USER",user.PASSWORD_USER)
            form_user.append("ID_QUYEN",0)
            form_user.append("GT_USER",user.GT_USER)
            form_user.append("HO_TEN_USER",user.HO_TEN_USER)
            form_user.append("AVATAR",user.AVATAR)
            form_user.append("BIRTH_DAY",user.BIRTH_DAY)
            form_user.append("SDT_USER",user.SDT_USER)
            form_user.append("DC_USER",user.DC_USER)

            axios.post('/api/user',form_user,{
              headers: {
                Authorization: token
              }
            })
            .then((response) => {
              resolve(response.data)
              commit("UPDATE_USER",user)
            })
            .catch((err) => {
              reject(err)
            })
          }
      })
    },
    //check username
    checkUsername({commit},USERNAME_USER)
    {
      console.log(USERNAME_USER)
      return new Promise((resolve,reject) => {
        axios.get(`/api/check-username?USERNAME_USER=${USERNAME_USER}`).then((response) => {
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
      })
    },

    fetchMenuUser({commit})
    {
      return new Promise((resolve,reject) => {
        axios.get('/api/user/menu',{
          headers: {
            Authorization: axios.defaults.params.token
          }
        })
        .then((response) => {
          commit("SET_MENU_USER",response.data.result)
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
      })
    },
    accessTokenEmail({commit},data)
    {
      return new Promise((resolve,reject) => {
        axios.post('/api/access-email',{
          ACCESS_TOKEN_EMAIL: data.ACCESS_TOKEN_EMAIL,
          USERNAME_USER: data.USERNAME_USER
        })
        .then((response) => {
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
      })
    },
    resetPassword({commit},data)
    {
      return new Promise((resolve,reject) => {
        axios.post('/api/reset-password',data)
        .then((response) => {
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
      })
    },
    updatePassword({commit},user)
    {
      return new Promise((resolve,reject) => {
        axios.post('/api/update-password/'+user.ACCESS_TOKEN_EMAIL,user)
        .then((response) => {
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
      })
    },
    checkPhone({commit},phone)
    {
      return new Promise((resolve,reject) => {
        if(phone != '')
        {
          axios.get('/api/check-phone/'+phone)
          .then((response) => {
            resolve(response.data)
          })
          .catch((err) => {
            reject(err)
          })
        }
        else
        {
          resolve({
            success: true,
            message: 'Số điện thoại rỗng',
            result: null
          })
        }
      })
    },

    registerFacebook({commit},data)
    {
      return new Promise((resolve,reject) => {
        axios.post('/api/register-facebook',data)
        .then((response) => {
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
      })
    },
    fetchUserStore({commit},filter)
    {   
      var filter_action = '';
      if(filter) filter_action = filter;
      return new Promise((resolve,reject) => {
        axios.get(`/api/user-store${filter_action}`,{
          headers: {
            Authorization: axios.defaults.params.token
          }
        })
        .then((response) => {
          if(response.success == true)
          {
            commit("SET_ACOUNT_STORE", response.data.result)
          }
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
      })
    },
    fetchRoleDefault({commit})
    {
      return new Promise((resolve,reject) => {
        axios.get('/api/role-default')
        .then((response) => {
          console.log('role default',response.data.result)
          commit('SET_ROLE_DEFAULT',response.data.result)
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
      })
    },
    //account store
    createAccountStore({commit},data)
    {
      return new Promise((resolve,reject) => {
        axios.post('/api/user-store',data,{
          headers: {
            Authorization: axios.defaults.params.token
          }
        })
        .then((response) => {
          if(response.data.success == true)
          {
            commit("ADD_ACCOUNT_STORE",data);
          }
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
      })
    },
    deleteAccountStore({commit},id)
    {
      return new Promise((resolve,reject) => {
        axios.delete(`/api/user-store/${id}`,{
          headers: {
            Authorization: axios.defaults.params.token
          }
        })
        .then((response) => {
          console.log(response.data)
          resolve(response.data)
        })
        .catch((err) => {
          reject(err)
        })
      })
    },
    //services
    fetchServicesDefault({commit})
    {
      return new Promise((resolve,reject) => {
        axios.get('/api/services-default')
        .then((response) => {
          if(response.data.success == true)
          {
            var service_store = response.data.value_default.filter((value,index,array) => {
              return array[index].NAME_ECOSY_CRM == 'UUID_SERVICES_DEFAULT_STORE'
            })[0]
            var service_store_default = response.data.result.filter((value,index,array) => {
              return array[index].UUID_SERVICES = service_store.VALUE_ECOSY_CRM
            })[0]
            service_store_default = JSON.stringify(service_store_default);
            console.log('services default', service_store_default)
            localStorage.setItem('service-store',service_store_default);
          }
          resolve(response.data)
        })
        .catch((err) => {
          localStorage.removeItem('services')
          reject(err)
        })
      })
    },
    updatePasswordUser({commit},user)
    {
      return new Promise((resolve,reject) => {
        axios.post('/api/update-password-user',user)
        .then((response) => {
          resolve(response.data);
        })
        .catch((err) => {
          reject(err);
        })
      })
    }
   
}