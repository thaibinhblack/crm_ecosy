export default {
    AUTH_SYSTEM: state => {
        return state.auth
    },
    LIST_USER: state => {
        return state.users
    },
    LIST_USER_ROLE: state => {
        return state.list_user_role
    },
    menu_user: state => {
        return state.menu_user
    },
    accounts_store: state => {
        return state.accounts_store
    },
    role_default: state => {
        return state.role_default
    },
    list_account_store: state => {
        return state.list_account_store
    }
}