export default {
    user: {},
    token: null,
    auth: {},
    users: [],
    info: {},
    list_user_role: [],
    menu_user: [],
    accounts_store: [],
    role_default: [],
    list_account_store: [],
    admin: false
}   