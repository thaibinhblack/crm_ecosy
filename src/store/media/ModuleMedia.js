import state from './ModuleStateMedia'
import getters from './ModuleGettersMedia'
import mutations from './ModuleMutationsMedia'
import actions from './ModuleActionsMedia'

export default {
    state,
    getters,
    mutations,
    actions
}