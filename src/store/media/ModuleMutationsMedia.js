export default {
    SET_MEDIA(state,media)
    {
        state.LIST_MEDIA = media
    },
    ADD_MEDIA(state,media)
    {
        state.LIST_MEDIA.unshift(media);
    },
    UPDATE_SIDEBAR(state,boolean)
    {
        if(boolean == false) {
            state.single = false;
            // state.media_result = []
        }
        state.sidebar = boolean
    },
    UPDATE_IMAGE_USER(state,boolean)
    {
        state.image_user_chose = boolean;
    },
    UPDATE_IMAGE_PRODUCT(state,boolean)
    {
        state.image_product = boolean
    },
    SET_IMAGE_USER(state,image)
    {
        state.image_user = image
    },
    ADD_MEDIA_RESULT(state,media_new)
    {
        const IndexMedia = state.media_result.findIndex((media) => media.ID_MEDIA == media_new.ID_MEDIA);
        console.log('add media',IndexMedia,media_new)
        if(IndexMedia == -1)
        {
            if(state.single == false)
            {
                state.media_result.push(media_new);
            }
            else
            {
                state.media_result = []
                state.media_result.push(media_new);
            }
        }
    },
    REMOVE_MEDIA_RESULT(state,media_old)
    {
        const IndexMedia = state.media_result.findIndex((media) => media.ID_MEDIA == media_old.ID_MEDIA);
        if(IndexMedia != -1)
        {
            state.media_result.splice(IndexMedia,1);
        }
    },
    SET_MEDIA_RESULT(state,media)
    {
        state.media_result = media
    },
    SINGLE_IAMGE(state,boolean)
    {
        state.single = boolean
    }
}