export default {
    LIST_MEDIA: state => {
        return state.LIST_MEDIA
    },
    media_result: state => {
        return state.media_result
    },
    open_close_sidebar: state => {
        return state.sidebar
    }
}