import axios from '@/axios'
import { resolve } from 'core-js/fn/promise'

export default {
    fetchMedia({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/media')
            .then((response) => {
                if(response.data.success == true) 
                {
                    commit("SET_MEDIA",response.data.result)
                }
                resolve(response.data)
            })
            .catch((err) => {
                console.log('media ',err)
                reject(err)
            })
        })
    },
    uploadMedia({commit},data)
    {
        return new Promise((resolve,reject) => {
            const formMedia = new FormData();
            formMedia.append("ID_CUA_HANG", data.ID_CUA_HANG)
            formMedia.append("TYPE_MEDIA", data.TYPE_MEDIA)
            formMedia.append("IMAGE_MEDIA", data.IMAGE_MEDIA)
            axios.post('/api/media',formMedia)
            .then((response) => {
                if(response.data.success == true)
                {
                    data.ID_MEDIA = response.data.result.ID_MEDIA;
                    data.FULL_URL = response.data.FULL_URL;
                    commit("ADD_MEDIA",data)
                }
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    }
}