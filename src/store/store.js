import Vue from 'vue'
import Vuex from 'vuex'

import state from "./state"
import getters from "./getters"
import mutations from "./mutations"
import actions from "./actions"
import moduleLocation from './location/moduleLocation'
Vue.use(Vuex)
import moduleStore from './store/moduleStore'
import moduleUser from './users/moduleUser'
import moduleCustomer from './customer/moduleCustomer'
export default new Vuex.Store({
    getters,
    mutations,
    state,
    actions,
    modules: {
        location: moduleLocation,
        user: moduleUser,
        store: moduleStore,
        customer: moduleCustomer
    },
    strict: process.env.NODE_ENV !== 'production'
})
