import state from './ModuleLandingPageState'
import getters from './ModuleLandingPageGetters'
import mutations from './ModuleLandingPageMutations'
import actions from './ModuleLandingPageActions'

export default {
    state,
    getters,
    mutations,
    actions
}