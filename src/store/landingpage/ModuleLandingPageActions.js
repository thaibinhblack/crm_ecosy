import axios from '@/axios'

export default {
    fetchMarkettingPublic({commit}, UUID)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/marketting-public/${UUID}`)
            .then((response) => {
                commit("SET_MARKETTING",response.data)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchProductMarketting({commit},UUID)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/marketting-public/${UUID}?type=product`)
            .then((response) => {
                commit("SET_PRODUCT",response.data)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchContactMarketting({commit},UUID)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/marketting-public/${UUID}?type=contact`)
            .then((response) => {
                commit("SET_CONTACT",response.data)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    }
}