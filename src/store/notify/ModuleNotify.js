import state from './ModuleNotifyState.js'
import getters from './ModuleNotifyGetters.js'
import mutations from './ModuleNotifyMutations.js'
import actions from './ModuleNotifyActions.js'

export default {
    state,
    getters,
    mutations,
    actions
}