import axios from '@/axios'
import { reject } from 'core-js/fn/promise';

export default {
    fetchNotify({commit})
    {
        return new Promise((resolve,reject) => {
            axios.get('/api/thong-bao',{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_DATA_NOTIFY",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    fetchCustomerByID_THONG_BAO({commit},ID_THONG_BAO)
    {
        return new Promise((resolve,reject) => {
            axios.get(`/api/thong-bao-kh/${ID_THONG_BAO}`,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                commit("SET_CUSTOMER_NOTIFY",response.data.result)
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    },
    createThongBao({commit},notify)
    {
        var form_data = new FormData();
        var type_thong_bao = ''
        notify.TYPE_THONG_BAO.forEach((type) => {
            type_thong_bao = type_thong_bao + type + ', '
        })
        form_data.append("ID_CUA_HANG",notify.ID_CUA_HANG);
        form_data.append("TITLE_THONG_BAO",notify.TITLE_THONG_BAO);
        form_data.append("CONTENT_THONG_BAO",notify.CONTENT_THONG_BAO);
        form_data.append("TYPE_THONG_BAO",type_thong_bao);
        form_data.append("LIST_CUSTOMER",notify.LIST_CUSTOMER);
        return new Promise((resolve,reject) => {
            axios.post('/api/thong-bao',form_data,{
                headers: {
                    Authorization: axios.defaults.params.token
                }
            })
            .then((response) => {
                resolve(response.data)
            })
            .catch((err) => {
                reject(err)
            })
        })
    }
}