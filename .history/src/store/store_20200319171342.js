/*=========================================================================================
  File Name: store.js
  Description: Vuex store
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import Vuex from 'vuex'

import state from "./state"
import getters from "./getters"
import mutations from "./mutations"
import actions from "./actions"
import moduleLocation from './location/moduleLocation'
Vue.use(Vuex)
import moduleStore from './store/moduleStore'
import moduleUser from './users/moduleUser'

export default new Vuex.Store({
    getters,
    mutations,
    state,
    actions,
    modules: {
        location: moduleLocation,
        user: moduleUser,
        store: moduleStore
    },
    strict: process.env.NODE_ENV !== 'production'
})
