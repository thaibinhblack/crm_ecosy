
import Vue from 'vue'
import Router from 'vue-router'
import axios from '@/axios.js'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior () {
        return { x: 0, y: 0 }
    },
    routes: [

        {
    // =============================================================================
    // MAIN LAYOUT ROUTES
    // =============================================================================
            path: '',
            component: () => import('./layouts/main/Main.vue'),
            children: [
        // =============================================================================
        // Theme Routes
        // =============================================================================
                {
                    path: '/',
                    redirect: '/stores'
                },
                {
                    path: '/stores',
                    name: 'DANH SÁCH CỬA HÀNG',
                    component: () => import('@/views/apps/store/DataStore.vue'),
                    meta: {
                        breadcrumb: [
                            { title: 'Home', url: '/' },
                            { title: 'Cửa hàng'},
                            { title: 'Danh sách cửa hàng', active: true },
                        ],
                        pageTitle: 'Cửa hàng',
                        rule: 'editor'
                    },
                },
                {
                    path: '/bills',
                    name: 'TRANG HÓA ĐƠN',
                    component: () => import('@/views/apps/bill/index.vue'),
                    children: [
                        {
                            path: '',
                            component: () => import('@/views/apps/bill/dataBill.vue'),
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách hóa đơn', active: true },
                                ],
                                pageTitle: 'Hóa đơn',
                                rule: 'editor'
                            },
                        },
                        {
                            path: '/add',
                            component: () => import('@/views/apps/bill/addBill.vue'),,
                            meta: {
                                breadcrumb: [
                                    { title: 'Home', url: '/' },
                                    { title: 'Danh sách hóa đơn', url: '/apps/bills'},
                                    { title: 'Thêm mới hóa đơn', active: true },
                                ],
                                pageTitle: 'Hóa đơn',
                                rule: 'editor'
                            },
                        }
                    ]
                   
                },

            ],
        },
    // =============================================================================
    // FULL PAGE LAYOUTS
    // =============================================================================
        {
            path: '',
            component: () => import('@/layouts/full-page/FullPage.vue'),
            children: [
        // =============================================================================
        // PAGES
        // =============================================================================
                {
                    path: '/callback',
                    name: 'auth-callback',
                    component: () => import('@/views/Callback.vue'),
                    meta: {
                        rule: 'editor'
                    }
                },
                {
                    path: '/login',
                    name: 'page-login',
                    component: () => import('@/views/pages/login/Login.vue'),
                    meta: {
                        rule: 'editor'
                    }
                },
                {
                    path: '/pages/register',
                    name: 'page-register',
                    component: () => import('@/views/pages/register/Register.vue'),
                    meta: {
                        rule: 'editor'
                    }
                },
              
              
                {
                    path: '/pages/error-404',
                    name: 'page-error-404',
                    component: () => import('@/views/pages/Error404.vue'),
                    meta: {
                        rule: 'editor'
                    }
                },
                {
                    path: '/pages/error-500',
                    name: 'page-error-500',
                    component: () => import('@/views/pages/Error500.vue'),
                    meta: {
                        rule: 'editor'
                    }
                },
            ]
        },
        // Redirect to 404 page, if no match found
        {
            path: '*',
            redirect: '/pages/error-404'
        }
    ],
})

router.afterEach(() => {
  // Remove initial loading
  const appLoading = document.getElementById('loading-bg')
    if (appLoading) {
        appLoading.style.display = "none";
    }
})

router.beforeEach((to, from, next) => {
    const app = Vue;
    console.log(app.$cookies.isKey('token'))
    if(app.$cookies.isKey('token'))
    {
        axios.defaults.params.token = app.$cookies.get('token')
    }
    else
    {
        to.path === '/login'
    }
    return next();

});

export default router
