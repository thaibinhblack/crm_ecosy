

export default [

  {
    header: "store",
    icon: "CartIcon",
    i18n: "storeSystem",
    items: [
      {
        url: null,
        name: "store",
        icon: "CartIcon",
        slug: "store",
        i18n: "store",
        submenu: [
          {
            url: '/stores',
            slug: 'stores',
            name: 'stores',
            i18n: 'stores'
          },
        ]
      },
    ]
  },

]

