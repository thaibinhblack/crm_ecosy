

export default [

  {
    header: "users",
    icon: "StoreIcon",
    i18n: "store",
    items: [
      {
        url: "/apps/users",
        name: "User",
        slug: "user",
        i18n: "stores",
      },
    ]
  },

]

