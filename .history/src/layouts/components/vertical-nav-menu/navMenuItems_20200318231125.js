

export default [

  {
    header: "store",
    icon: "StoreIcon",
    i18n: "store",
    items: [
      {
        url: null,
        name: "store",
        slug: "store",
        i18n: "store",
        submenu: [
          {
            url: '/stores',
            slug: 'stores',
            name: 'stores',
            i18n: 'stores'
          },
        ]
      },
    ]
  },

]

