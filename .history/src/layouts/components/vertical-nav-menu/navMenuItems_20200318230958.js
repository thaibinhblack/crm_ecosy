

export default [

  {
    header: "users",
    icon: "StoreIcon",
    i18n: "store",
    items: [
      {
        url: "/apps/stores",
        name: "User",
        slug: "user",
        i18n: "stores",
      },
    ]
  },

]

