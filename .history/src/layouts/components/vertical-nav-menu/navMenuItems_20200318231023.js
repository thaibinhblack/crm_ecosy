

export default [

  {
    header: "store",
    icon: "StoreIcon",
    i18n: "store",
    items: [
      {
        url: "/stores",
        name: "stores",
        slug: "stores",
        i18n: "stores",
      },
    ]
  },

]

