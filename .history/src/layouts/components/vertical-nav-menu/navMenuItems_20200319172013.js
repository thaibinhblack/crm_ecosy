

export default [

  {
    header: "store",
    icon: "CartIcon",
    i18n: "storeSystem",
    items: [
      {
        url: null,
        name: "store",
        icon: "CartIcon",
        slug: "store",
        i18n: "store",
        submenu: [
          {
            url: '/stores',
            slug: 'stores',
            name: 'stores',
            i18n: 'stores'
          },
        ]
      },
      {
        url: null,
        name: 'Hóa đơn',
        i18n: 'bill',
        icon: 'BillIcon',
        submenu: [
          {
            url: '/bills',
            slug: 'bills',
            name: 'Danh sách hóa đơn',
            i18n: 'bills'
          },
          {
            url: '/bills/add',
            slug: 'add-bill',
            name: 'Thêm mới hóa đơn',
            i18n: 'addBill'
          }
        ]
      }
    ]
  },

]

