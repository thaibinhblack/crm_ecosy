// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
    apiKey: "AIzaSyBm2rMz9Y_CsdV3TLrGZaD_aZvWOm1B1Fw",
    authDomain: "ecosy-22213.firebaseapp.com",
    databaseURL: "https://ecosy-22213.firebaseio.com",
    projectId: "ecosy-22213",
    storageBucket: "ecosy-22213.appspot.com",
    messagingSenderId: "176570185287",
    appId: "1:176570185287:web:4600c476b6306601726362",
    measurementId: "G-E9K3HF7905"
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = 'Background Message Title';
    const notificationOptions = {
      body: 'Background Message body.',
      icon: '/firebase-logo.png'
    };
  
    return self.registration.showNotification(notificationTitle,
        notificationOptions);
  });